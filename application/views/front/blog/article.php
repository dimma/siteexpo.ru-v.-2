<div class="content">
	<div class="wrap">
		<h1><?=$post->title?></h1>
		<p class="time"><?=Date::format($post->date, 'd F y')?><? /* <span class="">Комментарии&nbsp;8</span> */ ?></p>
		<div class="pic inner">
			<img src="/assets/upload/blog/<?=$post->images->order_by('date','DESC')->limit(1)->find()->file?>" alt="">
		</div>
		<?=$post->text?>
	</div>
</div>