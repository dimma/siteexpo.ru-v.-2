<div class="wrapper">
	<div class="content">
		<? foreach($posts as $post) { ?>
			<section>
				<a href="/blog/<?=$post->url?>">
					<h2><?=$post->title?></h2>
					<p class="time"><?=Date::format($post->date, 'd F Y')?><? if($post->comments_count) { ?><span class="">Комментарии&nbsp;<?=$post->comments_count?></span><? } ?></p>
					<div class="pic">
						<img src="/assets/upload/blog/<?=$post->images->order_by('date','DESC')->limit(1)->find()->file?>" alt="">
					</div>
				</a>
				<p><?=Text::limit_chars($post->text, 350, '&hellip;')?></p>
			</section>
		<? } ?>
	</div>
	<div class="push"></div>
</div>