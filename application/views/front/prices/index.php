<div class="wrapper">
	<div class="wrap">
		<div class="selection">
			<h2>Какой сайт вы хотите?</h2>
			<ul class="rate">
				<li>
					<div class="fotoimg-wrap">
						<div class="fotoImg middle"></div>
						<div class="fotoImg middle active-img"></div>
					</div>
					<h4>Корпоративный</h4>
					<p>Корпоративный сайт — важная имиджевая составляющая любого бизнеса.</p>
				</li>
				<li>
					<div class="fotoimg-wrap">
						<div class="fotoImg left"></div>
						<div class="fotoImg left active-img"></div>
					</div>
					<h4>Интернет-магазин</h4>
					<p>Актуальный и эффективный инструмент продажи товаров и услуг через интернет.</p>
				</li>
				<li>
					<div class="fotoimg-wrap">
						<div class="fotoImg right"></div>
						<div class="fotoImg right active-img"></div>
					</div>
					<h4>Стартап</h4>
					<p>Идеально подходит для крупных проектов с большим количеством пользователей.</p>
				</li>
			</ul>
		</div>

		<div class="clear"></div>

		<div class="info" id="info">
		<div class="bd">
			<div class="triangle" id="triangle"></div>
			<div class="bd-wrap">
				<div id="details-wrap">
					<div id="tab1" class="details-info">
						<div class="detail">
							<h4 class="_title">Бюджетный</h4>
							<h4><?=Num::format($base_price['corporate_budget']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['corporate'][1], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-3"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['corporate']['base'][1] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_4">
									<label for="title_4">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['corporate']['additional'][1] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого <span class="total-price" base_price="<?=$base_price['corporate_budget']['value']?>"><?=Num::format($base_price['corporate_budget']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание бюджетного корпоративного сайта. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="detail">
							<h4 class="_title">Стандартный</h4>
							<h4><?=Num::format($base_price['corporate_standart']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['corporate'][2], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-4"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['corporate']['base'][2] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_5">
									<label for="title_5">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['corporate']['additional'][2] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого <span class="total-price" base_price="<?=$base_price['corporate_standart']['value']?>"><?=Num::format($base_price['corporate_standart']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание cтандартного корпоративного сайта. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="detail">
							<h4 class="_title">Уникальный</h4>
							<h4><?=Num::format($base_price['corporate_unique']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['corporate'][3], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-5"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['corporate']['base'][3] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_6">
									<label for="title_6">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['corporate']['additional'][3] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого <span class="total-price" base_price="<?=$base_price['corporate_unique']['value']?>"><?=Num::format($base_price['corporate_unique']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание уникального корпоративного сайта. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="zakaz">
							<div><button class="trans bud arrow-corner" category="corporate" type="1">Заказать бюджетный</button></div>
							<div><button class="color st arrow-corner" category="corporate" type="2">Заказать стандартный</button></div>
							<div><button class="trans un arrow-corner" category="corporate" type="3">Заказать уникальный</button></div>
						</div>

					</div><!-- TAB 2 -->

					<div id="tab2" class="details-info">
						<div class="detail">
							<h4 class="_title">Бюджетный</h4>
							<h4><?=Num::format($base_price['shop_budget']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['shop'][1], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-3"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['shop']['base'][1] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_1">
									<label for="title_1">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['shop']['additional'][1] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого: <span class="total-price" base_price="<?=$base_price['shop_budget']['value']?>"><?=Num::format($base_price['shop_budget']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание бюджетного интернет-магазина. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="detail">
							<h4 class="_title">Стандартный</h4>
							<h4><?=Num::format($base_price['shop_standart']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['shop'][2], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-4"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['shop']['base'][2] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_2">
									<label for="title_2">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['shop']['additional'][2] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого: <span class="total-price" base_price="<?=$base_price['shop_standart']['value']?>"><?=Num::format($base_price['shop_standart']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание стандартного интернет-магазина. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="detail">
							<h4 class="_title">Уникальный</h4>
							<h4><?=Num::format($base_price['shop_unique']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['shop'][3], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-5"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['shop']['base'][3] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_3">
									<label for="title_3">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['shop']['additional'][3] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого: <span class="total-price" base_price="<?=$base_price['shop_unique']['value']?>"><?=Num::format($base_price['shop_unique']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание уникального интернет-магазина. Все цены указаны в рублях.</h6>
							</div>
						</div>

						<div class="zakaz">
							<div><button class="trans bud arrow-corner" category="shop" type="1">Заказать бюджетный</button></div>
							<div><button class="color st arrow-corner" category="shop" type="2">Заказать стандартный</button></div>
							<div><button class="trans un arrow-corner" category="shop" type="3">Заказать уникальный</button></div>
						</div>
					</div><!-- TAB 1 -->

					<div id="tab3" class="details-info">
						<div class="detail">
							<h4 class="_title">Бюджетный</h4>
							<h4><?=Num::format($base_price['startup_budget']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['startup'][1], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-3"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['startup']['base'][1] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_7">
									<label for="title_7">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['startup']['additional'][1] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого <span class="total-price" base_price="<?=$base_price['startup_budget']['value']?>"><?=Num::format($base_price['startup_budget']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание бюджетного стартапа. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="detail">
							<h4 class="_title">Стандартный</h4>
							<h4><?=Num::format($base_price['startup_standart']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['startup'][2], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-4"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['startup']['base'][2] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_8">
									<label for="title_8">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['startup']['additional'][2] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого <span class="total-price" base_price="<?=$base_price['startup_standart']['value']?>"><?=Num::format($base_price['startup_standart']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание стандартного стартапа. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="detail">
							<h4 class="_title">Уникальный</h4>
							<h4><?=Num::format($base_price['startup_unique']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['startup'][3], 0, ',', ' ')?> <span class="rub">a</span></h4>
							<div class="stars">
								<span class="star s-5"></span>
							</div>
							<div class="list">
								<h4>Базовые функции</h4>
								<ul>
									<? foreach($services['startup']['base'][3] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h4>
									<input type="checkbox" class="additional" id="title_9">
									<label for="title_9">Дополнительно</label>
								</h4>
								<ul class="_add">
									<? foreach($services['startup']['additional'][3] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h4>Итого <span class="total-price" base_price="<?=$base_price['startup_unique']['value']?>"><?=Num::format($base_price['startup_unique']['value'], 0, ',', ' ')?></span> <span class="rub">a</span></h4>
								<h6>Цена на создание уникального стартапа. Все цены указаны в рублях.</h6>
							</div>
						</div>
						<div class="zakaz">
							<div><button class="trans bud arrow-corner" category="startup" type="1">Заказать бюджетный</button></div>
							<div><button class="color st arrow-corner" category="startup" type="2">Заказать стандартный</button></div>
							<div><button class="trans un arrow-corner" category="startup" type="3">Заказать уникальный</button></div>
						</div>

					</div><!-- TAB 3 -->
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="push"></div>
</div>

<!-- MODAL WINDOW -->

<div class="modalBg" id="modal-request-bg"></div>
<div class="modalWin" id="modal-request">
	<div class="request-wrap">
		<h2>Оставить заявку<br></h2>
		<h4>на <span class="site-type"></span> <span class="site-category"></span></h4>
		<!--<div class="stars"><span class="star s-5"></span></div>-->
		<div class="form_input">
			<form action="/contact" method="post" accept-charset="utf-8" class="contact-form">
				<input type="hidden" name="request" value="1">
				<input type="hidden" name="services">
				<input type="hidden" name="category">
				<input type="hidden" name="type">

				<p><label for="name">Имя</label><input type="text" name="name" placeholder=""></p>
				<p><label for="email">Почта</label><input type="text" name="email" placeholder=""></p>
				<p><label for="phone">Телефон</label><input type="text" class="phone" name="phone" placeholder="+7 987 654-32-10 — пример"></p>
				<button class="arrow-corner">Отправить заявку</button>
				<button class="_cansel">Отменить</button>
			</form>
		</div>
		<div class="form-errors">
			<p class="error-name error-txt">Укажите ваше имя</p>
			<p class="error-email error-txt">Неправильный адрес электронной почты</p>
			<p class="error-phone error-txt">Неправильный телефон</p>
		</div>
		<div class="ps">
			<p>Сразу после того, как мы получим вашу заявку, наш менеджер свяжется с вами для уточнения деталей.</p>
			<br>
			<p>Вы можете не заполнять заявку.</p>
			<p>Просто позвоните нам:</p>
			<p>+7 (495) 374-64-82</p>
			<p>8 800 500 73 28</p>
		</div>
	</div>
</div>
<!-- END MODAL WINDOW -->
