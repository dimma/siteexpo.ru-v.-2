<section id="portfolio">
	<div class="wrap">
		<h2>Портфолио</h2>
		<p>Наши сайты увеличивают продажи. Мы сами занимаемся продвижением сайтов, создаем фирменные стили и оказываем рекламную поддержку проектов. <a href="/portfolio">Посмотреть все проекты</a></p>
		<div class="slider">
			<a class="prewButton" href="#"></a>
			<a class="nextButton" href="#"></a>
			<div class="slider-wrap">
				<div class="slider-inner">
					<!--<div class="slide"><img src="/assets/img/slider/orbios.jpg" alt=""></div>-->
					<div class="slide"><img src="/assets/img/slider/xsoft.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/slider/roombion.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/slider/ted.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/slider/pizza.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/slider/normapravo.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/slider/zioniti.jpg" alt=""></div>
				</div>
			</div>
			<div class="description">
				<!--<div class="project-desc">
					<span>Облачный сервис для хранения файлов и работы над совместными проектами.</span>
					<p>orbios.com</p>
				</div>-->
				<div class="project-desc">
					<span>&nbsp;</span>
					<p>xsoft.org</p>
				</div>
				<div class="project-desc" style="display: none">
					<span>Стильный интернет–магазин товаров для дома.</span>
					<p>roombion.ru</p>
				</div>
				<div class="project-desc" style="display: none">
					<span>Сайт для крупной логистической компании Transport Evolution Dynamics.</span>
					<p>tedtrans.com</p>
				</div>
				<div class="project-desc" style="display: none">
					<span>Вкусная пицца, аппетитный сайт.</span>
					<p>pizzarion.ru</p>
				</div>
				<div class="project-desc" style="display: none">
					<span>Юридический центр защиты населения и бизнеса</span>
					<p>normapravo.ru</p>
				</div>
				<div class="project-desc" style="display: none">
					<h6>Безопасная почта</h6>
					<p>zioniti.com</p>
				</div>
			</div>

		</div>

	</div>
</section>

<section id="disposal">
	<div class="wrap">
		<h2>Услуги</h2>
		<p>Быстрые и функциональные решения для вашего бизнеса.<br>
			<!--<a href="/prices">Подробнее об услугах</a></p>-->
		<ul>
			<li>
				<img src="/assets/img/disposal-icon-1.svg" alt="">
				<h3>Разработка сайтов</h3>
				<p>Хотите интернет–магазин, имиджевый или промо–сайт, корпоративный сайт? Мы работаем с разными бюджетами и готовы реализовать любую вашу идею. <!--<a href="/prices">Подробнее</a>--></p>
			</li>
			<li>
				<img src="/assets/img/disposal-icon-2.svg" alt="">
				<h3>Фирменный стиль</h3>
				<p>Если вы хотите, чтобы ваш бренд был узнаваем, вам нужен запоминающийся фирменный стиль. Мы разрабатываем фирменные стили с нуля и обновляем уже существующие стили. <!--<a href="/prices">Подробнее</a>--></p>
			</li>
			<li>
				<img src="/assets/img/disposal-icon-3.svg" alt="">
				<h3>Мобильные приложения</h3>
				<p>Мы создаем современные мобильные приложения для IOS и Android и самостоятельно публикуем их в Google Play Store и Apple App Store от вашего имени. <!--<a href="/prices">Подробнее</a>--></p>
			</li>
			<li>
				<img src="/assets/img/disposal-icon-4.svg" alt="">
				<h3>Продвижение</h3>
				<p>Мы проводим маркетинговые акции, продвигаем бренды в социальных сетях и анализируем статистику использования сайтов пользователями. <!--<a href="/prices">Подробнее</a>--></p>
			</li>
		</ul>
	</div>
</section>

<section id="team">
	<div class="wrap">
		<h2>Команда</h2>
		<p>Сплоченная команда настоящих профессионалов.<br> Приезжайте знакомиться.</p>
		<ul>
			<li>
				<img src="/assets/img/f-1.png" alt="">
				<span class="team-name">Мугутдин</span>
				<br/>
				<span class="team-desc">Генеральный директор</span>
			</li>
			<li>
				<img src="/assets/img/f-2.png" alt="">
				<span class="team-name">Кирилл</span>
				<br/>
				<span class="team-desc">Менеджер проектов</span>
			</li>
			<li>
				<img src="/assets/img/f-3.png" alt="">
				<span class="team-name">Дмитрий</span>
				<br/>
				<span class="team-desc">Арт-директор</span>
			</li>
			<li>
				<img src="/assets/img/f-4.png" alt="">
				<span class="team-name">Милош</span>
				<br/>
				<span class="team-desc">Проектировщик</span>
			</li>
			<li>
				<img src="/assets/img/f-5.png" alt="">
				<span class="team-name">Артем</span>
				<br/>
				<span class="team-desc">Копирайтер</span>
			</li>
			<li>
				<img src="/assets/img/f-6.png" alt="">
				<span class="team-name">Николай</span>
				<br/>
				<span class="team-desc">Технолог</span>
			</li>
			<li>
				<img src="/assets/img/f-7.png" alt="">
				<span class="team-name">Вадим</span>
				<br/>
				<span class="team-desc">Программист</span>
			</li>
			<li>
				<img src="/assets/img/f-8.png" alt="">
				<span class="team-name">Максим</span>
				<br/>
				<span class="team-desc">Дизайнер</span>
			</li>
			<li>
				<img src="/assets/img/f-9.png" alt="">
				<span class="team-name">Рустам</span>
				<br/>
				<span class="team-desc">Программист</span>
			</li>
			<li>
				<img src="/assets/img/f-10.png" alt="">
				<span class="team-name">Антон</span>
				<br/>
				<span class="team-desc">СЕО</span>
			</li>
			<li class="send-cv">
				<a href="mailto:info@siteexpo.ru?Subject=Resume">
					<img src="/assets/img/bd.svg" alt="">
					<span class="team-name">Дизайнер</span>
					<br/>
					<span class="team-desc">Отправить резюме</span>
				</a>
			</li>
		</ul>
	</div>
</section>

<section id="contact">
	<div class="wrap">
		<div class="contacts">
			<div class="contact-details">
				<p>+7 (495) 374-64-82</p>
				<p>8 800 500 73 28</p>
				<p><a href="mailto:info@siteexpo.ru?Subject=Zakaz">info@siteexpo.ru</a></p>
			</div>
			<ul class="address">
				<li><p>Наш офис в Москве:</p></li>
				<li><p>Москва, Пресненская набережная, д. 12</p></li>
				<li><p>ММДЦ «Москва Сити», комплекс Федерация,</p></li>
				<li><p>башня Восток, 31 этаж, офис Q</p></li>
			</ul>
			<div class="clear"></div>
			<ul class="social">
				<li><a target="_blank" href="https://www.facebook.com/siteexpo2"><img src="/assets/img/fb.png" alt=""></a></li>
				<!--<li><a href="#"><img src="/assets/img/vk.png" alt=""></a></li>
				<li><a href="#"><img src="/assets/img/in.png" alt=""></a></li>
				<li><a href="#"><img src="/assets/img/ball.png" alt=""></a></li>
				<li><a href="#"><img src="/assets/img/be.png" alt=""></a></li>
				<li><a href="#"><img src="/assets/img/inst.png" alt=""></a></li>-->
				<li><a target="_blank" href="https://twitter.com/SiteexpoRu"><img src="/assets/img/twitter.png" alt=""></a></li>
			</ul>
			<div class="clear"></div>
			<p class="copy">ООО «СайтЭкспо», XSoft ltd. &copy; 2014</p>
		</div>
		<div class="map"><a href="/contacts"><img src="/assets/img/mask.png" alt=""></a></div>
	</div>
</section>