<!DOCTYPE html>
<html>
	<head>
		<title><?=$title?></title>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/x-icon" href="/favicon.ico" />

		<? if($apple) { ?>
			<meta name="viewport" content="width=device-width">
		<? } else { ?>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<? } ?>

		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<meta content="telephone=no" name="format-detection">
		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="apple-touch-icon" href="siteexpo_touch-icon-iphone.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="siteexpo_touch-icon-ipad.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="siteexpo_touch-icon-iphone-retina.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="siteexpo_touch-icon-ipad-retina.png" />

		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
		<!--[if gte IE 9]>
		<style type="text/css">
			.gradient { filter: none; }
		</style>
		<![endif]-->
		<!--[if lte IE 8]>
		<meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
		<![endif]-->
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body class="<?php echo $body_class; ?>">
		<? if($main_page) { ?>
			<section id="preview">
				<div class="default" id="nav">
					<div class="wrap">
						<ul>
							<li><a href="/"><img src="/assets/img/logo.svg" id="nav-logo-fixed" alt=""></a></li>

							<ul class="centering">
							<? foreach($menu as $k => $v) { ?>
								<li><a <? if($controller == $v['url']) { ?>class="active"<? } ?> href="/<?=$v['url']?>"><?=$v['name']?></a></li>
							<? } ?>
							</ul>

							<li class="entry-form arrow-corner">
								<a href="#">Оставить заявку</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="wrap">
					<h1>Интернет–решения <br>для бизнеса</h1>
					<h3>Мы разрабатываем сайты, мобильные приложения для<br> IOS и Android. Создаем фирменные стили для компаний и занимаемся комплексным продвижением бизнеса через интернет.</h3>
				</div>
			</section>
			<?=$content?>
		<? } else { ?>
			<!-- Navigation
			================================================== -->
			<div class="fixed" id="nav">
				<div class="wrap">
					<ul>
						<li><a href="/"><img src="/assets/img/logoFix.svg" id="nav-logo-fixed" alt=""></a></li>

						<ul class="centering">
						<? foreach($menu as $k => $v) { ?>
						<li><a <? if($controller == $v['url']) { ?>class="active"<? } ?> href="/<?=$v['url']?>"><?=$v['name']?></a></li>
						<? } ?>
						</ul>

						<li class="entry-form arrow-corner">
							<a href="#">Оставить заявку</a>
						</li>
					</ul>
				</div>

				<? /* Contacts page */ ?>
				<? if($controller == 'contacts' && $action == 'index') { ?>
				<div class="contacts">
					<div class="contact-details">
						<p>+7 (495) 374-64-82</p>
						<p>8 800 500 73 28</p>
						<p><a href="mailto:info@siteexpo.ru?Subject=Zakaz">info@siteexpo.ru</a></p>
					</div>
					<ul class="address">
						<li><p>Наш офис в Москве:</p></li>
						<li><p>Москва, Пресненская набережная, д. 12</p></li>
						<li><p>ММДЦ &laquo;Москва Сити&raquo;, комплекс Федерация,</p></li>
						<li><p>башня Восток, 31 этаж, офис Q</p></li>
					</ul>
				</div>
				<? } ?>
			</div>

			<!-- Content
			================================================== -->
			<?=$content?>

			<? /* Contacts page */ ?>
			<? if($controller == 'contacts' && $action == 'index') { ?>
				<div class="map" id="contacts-map"></div>
			<? } ?>

			<!-- Footer
			================================================== -->
			<? if($controller !== 'contacts') { ?>
			<div class="footer">
				<div class="wrap">
					<div class="contacts">
						<ul class="social">
							<li><a target="_blank" href="https://www.facebook.com/siteexpo2"><img src="/assets/img/fb.png" alt=""></a></li>
							<!--<li><a href="#"><img src="/assets/img/vk.png" alt=""></a></li>
							<li><a href="#"><img src="/assets/img/in.png" alt=""></a></li>
							<li><a href="#"><img src="/assets/img/ball.png" alt=""></a></li>
							<li><a href="#"><img src="/assets/img/be.png" alt=""></a></li>
							<li><a href="#"><img src="/assets/img/inst.png" alt=""></a></li>-->
							<li><a target="_blank" href="https://twitter.com/SiteexpoRu"><img src="/assets/img/twitter.png" alt=""></a></li>
						</ul>
						<div class="contact-details">
							<p>+7 (495) 374-64-82</p>
							<p>8 800 500 73 28</p>
							<p><a href="mailto:info@siteexpo.ru?Subject=Zakaz">info@siteexpo.ru</a></p>
						</div>
						<p class="copy">ООО «СайтЭкспо», XSoft ltd. &copy; 2014</p>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<? } ?>
		<? } ?>

		<div class="modal">
			<div class="modalBg" id="top-req-bg"></div>
			<div class="modalWin" id="top-req-win">
				<h2>Оставьте заявку</h2>
				<div class="form_input">
					<form action="/contact" method="post" accept-charset="utf-8" id="contact-form" class="contact-form">
						<p><label for="name">Имя</label><input type="text" name="name" placeholder=""></p>
						<p><label for="email">Почта</label><input type="text" name="email" placeholder=""></p>
						<p><label for="phone">Телефон</label><input type="text" class="phone" name="phone" placeholder="+7 987 654-32-10 — пример"></p>
						<button class="arrow-corner">Отправить заявку</button>
						<button class="_cansel">Отменить</button>
					</form>
				</div>
				<div class="form-errors">
					<p class="error-name error-txt">Укажите ваше имя</p>
					<p class="error-email error-txt">Неправильный адрес электронной почты</p>
					<p class="error-phone error-txt">Неправильный телефон</p>
				</div>
			</div>
		</div>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter24951449 = new Ya.Metrika({id:24951449,
							webvisor:true,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="//mc.yandex.ru/watch/24951449" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-50888095-1', 'auto');
			ga('send', 'pageview');
		</script>

		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>