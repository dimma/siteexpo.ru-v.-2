<div class="wrapper">
	<div class="wrap">
		<ul class="block-preview" id="portfolio-list">
			<? foreach($cases as $case) { ?>
			<li>
				<a href="portfolio/<?=$case->url?>">
					<img src="/assets/img/cases/<?=$case->id?>s.png" alt="">
					<h4><?=$case->name?></h4>
					<h6><?=$case->desc?></h6>
				</a>
			</li>
			<? } ?>
		</ul>
		<div class="clear"></div>
	</div>
	<div class="push"></div>
</div>