<? /* XSOFT */ ?>
<div class="wrapper">
	<div class="wrap">
		<img class="logo-project" src="/assets/img/projects/xsoft/logo-xsoft.png" alt="">
		<h2>Разработка сайта для IT компании</h2>
		<p>Мы разработали сайт для современной российской IT–компании. Мы решили сделать сайт простым, понятным, не перенасыщенным контентом.</p>
	</div>

		<div class="bloc half">
			<img src="/assets/img/projects/xsoft/page.jpg" alt="">
		</div>
		<div class="bloc"></div>
		<div class="slider-outer">
		<div class="slider pf">
			<a class="prewButton" href="#"></a>
			<a class="nextButton" href="#"></a>
			<div class="slider-wrap">
				<div class="slider-inner">
					<div class="slide"><img src="/assets/img/projects/xsoft/1.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/xsoft/2.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/xsoft/3.jpg" alt=""></div>
				</div>
			</div>
			<div class="description">
				<div class="project-desc">
					<h6>Мы сняли сотрудников компании в процессе работы над проектами и отказались от шаблонных текстов вроде «мы молодая, динамично развивающаяся компания».</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Выдвигающееся боковое меню.</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Модальное окно для отклика на вакансии.</h6>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap"></div>
	<div class="bd"><h4>Минималистичные иконки</h4></div>
	<div class="bloc icons xs">
		<img src="/assets/img/projects/xsoft/icons.png" alt="">
	</div>
</div>
