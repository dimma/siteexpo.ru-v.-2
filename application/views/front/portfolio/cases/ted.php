<? /* TED */ ?>
<div class="wrapper">
	<div class="wrap">
		<img class="logo-project" src="/assets/img/projects/ted/logo-ted.png" alt="">
		<h2>Дизайн и разработка сайта<br> международной логистической компании<br> Transport Evolution Dynamics</h2>
	</div>
	<div class="slider-outer">
		<div class="slider pf">
			<a class="prewButton" href="#"></a>
			<a class="nextButton" href="#"></a>
			<div class="slider-wrap">
				<div class="slider-inner">
					<div class="slide"><img src="/assets/img/projects/ted/1.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/ted/2.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/ted/3.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/ted/4.jpg" alt=""></div>
				</div>
			</div>
			<div class="description">
				<div class="project-desc">
					<h6>Динамическая шапка сайта показывает способы доставки грузов.</h6>
				</div>
				<div class="project-desc">
					<h6>Динамическая шапка сайта показывает способы доставки грузов.</h6>
				</div>
				<div class="project-desc">
					<h6>Динамическая шапка сайта показывает способы доставки грузов.</h6>
				</div>
				<div class="project-desc">
					<h6>Динамическая шапка сайта показывает способы доставки грузов.</h6>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="bloc">
			<h4>Интерактивная карта</h4>
			<p>На главной странице мы разметили интерактивную карту с изображением маршрутов перевозок.</p>
			<img src="/assets/img/projects/ted/map.png" alt="">
		</div>
		<div class="bloc">
			<h4>Внутренние страницы</h4>
			<p>На внутренних страницах представлена подробная информация о географии перевозок и услугах компании.</p>
			<ul>
				<li>
					<h6>Главная</h6>
					<img src="/assets/img/projects/ted/01.jpg" alt="">
				</li>
				<li>
					<h6>География</h6>
					<img src="/assets/img/projects/ted/02.jpg" alt="">
				</li>
				<li>
					<h6>Виды перевозок</h6>
					<img src="/assets/img/projects/ted/03.jpg" alt="">
				</li>
			</ul>
			<div class="clear"></div>
			<ul>
				<li>
					<h6>Таможенное оформление</h6>
					<img src="/assets/img/projects/ted/04.jpg" alt="">
				</li>
				<li>
					<h6>О компании</h6>
					<img src="/assets/img/projects/ted/05.jpg" alt="">
				</li>
				<li>
					<h6>Контакты</h6>
					<img src="/assets/img/projects/ted/06.jpg" alt="">
				</li>
			</ul>
			<div class="clear"></div>
		</div>

		<div class="bloc" style="width:850px;">
			<h4>Профессиональная фотосессия</h4>
			<p>Мы сфотографировали сотрудников компании TED для страницы &quot;О компании&quot;.</p>
			<ul>
				<li>
					<img src="/assets/img/projects/ted/f1.jpg" alt="">
				</li>
				<li>
					<img src="/assets/img/projects/ted/f2.jpg" alt="">
				</li>
				<li>
					<img src="/assets/img/projects/ted/f3.jpg" alt="">
				</li>
			</ul>
			<div class="clear"></div>
			<ul>
				<li>
					<img src="/assets/img/projects/ted/f4.jpg" alt="">
				</li>
				<li>
					<img src="/assets/img/projects/ted/f5.jpg" alt="">
				</li>
				<li>
					<img src="/assets/img/projects/ted/f6.jpg" alt="">
				</li>
			</ul>
			<div class="clear"></div>
			<ul>
				<li>
					<img src="/assets/img/projects/ted/f7.jpg" alt="">
				</li>
				<li>
					<img src="/assets/img/projects/ted/f8.jpg" alt="">
				</li>
				<li>
					<img src="/assets/img/projects/ted/f9.jpg" alt="">
				</li>
			</ul>
			<div class="clear"></div>
		</div>
	</div>
	<div class="bd"><h4>Набор иконок для сайта</h4></div>
	<div class="bloc icons">
		<img src="/assets/img/projects/ted/icons.png" alt="">
	</div>
</div>
