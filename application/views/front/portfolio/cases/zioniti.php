<? /* ZIONITI */ ?>
<div class="wrapper">
	<div class="wrap">
		<img class="logo-project" src="/assets/img/projects/zioniti/logo-zioniti.png" alt="">
		<h2>Zioniti — безопасная почта</h2>
		<p>Главная страница информационного сайта, рассказывающего о новом безопасном почтовом клиенте.</p>
	</div>

		<div class="bloc half">
			<img src="/assets/img/projects/zioniti/page.jpg" alt="">
		</div>

		<div class="slider-outer">
		<div class="slider pf">
			<a class="prewButton" href="#"></a>
			<a class="nextButton" href="#"></a>
			<div class="slider-wrap">
				<div class="slider-inner">
					<div class="slide"><img src="/assets/img/projects/zioniti/1.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/zioniti/2.jpg" alt=""></div>
				</div>
			</div>
			<div class="description">
				<div class="project-desc">
					<h6>Телефоны и адрес службы поддержки с возможностью заказать обратный звонок.</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Видеоролик, рассказывающий о преимуществах безопасной почты Zioniti.</h6>
				</div>
			</div>
		</div>
	</div>
	<div class="bloc">
		<h4>Основные преимущества Zioniti в картинках</h4>
		<p></p>
		<div class="bloc half">
			<img src="/assets/img/projects/zioniti/info1.png" alt="">
			<p></p>
			<p>Сверхзащищенная переписка</p>
		</div>
		<div class="bloc half">
			<img src="/assets/img/projects/zioniti/info2.png" alt="">
			<p></p>
			<p>Надежная защита данных</p>
		</div>
		<div class="bloc half">
			<img src="/assets/img/projects/zioniti/info3.png" alt="">
			<p></p>
			<p>Двухфакторная аутентификация</p>
		</div>
	</div>
	<div class="wrap" style="padding-top: 0">
		<div class="bloc null">
			<h4>Внутренние страницы</h4>
			<p></p>
			<ul>
				<li>
					<h6>Продукты</h6>
					<img src="/assets/img/projects/zioniti/01.jpg" alt="">
				</li>
				<li>
					<h6>Бизнес-решения</h6>
					<img src="/assets/img/projects/zioniti/02.jpg" alt="">
				</li>
				<li>
					<h6>Страница загрузки Zioniti</h6>
					<img src="/assets/img/projects/zioniti/03.jpg" alt="">
				</li>
			</ul>
		</div>
		<div class="clear"></div>
		<div class="bloc">
			<h4>Страницы тарифов подробно рассказывают о каждом из тарифных планов</h4>
			<p></p>
			<ul>
				<li>
					<h6>Zioniti LITE</h6>
					<img src="/assets/img/projects/zioniti/04.jpg" alt="">
				</li>
				<li>
					<h6>Zioniti PRO</h6>
					<img src="/assets/img/projects/zioniti/05.jpg" alt="">
				</li>
				<li>
					<h6>Zioniti SERVER</h6>
					<img src="/assets/img/projects/zioniti/06.jpg" alt="">
				</li>
			</ul>
		</div>
		<div class="clear"></div>
		<div class="bloc">
			<h4>Страницы покупки тарифных планов</h4>
			<p>Динамическое изменение цены в  зависимости от количества комплектов или сотрудников компании.</p>
			<ul>
				<li>
					<h6>Zioniti LITE</h6>
					<img src="/assets/img/projects/zioniti/07.jpg" alt="">
				</li>
				<li>
					<h6>Zioniti PRO</h6>
					<img src="/assets/img/projects/zioniti/08.jpg" alt="">
				</li>
				<li>
					<h6>Zioniti SERVER</h6>
					<img src="/assets/img/projects/zioniti/09.jpg" alt="">
				</li>
			</ul>
		</div>
	</div>
</div>
