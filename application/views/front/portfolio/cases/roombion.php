<? /* Roombion */ ?>
<div class="wrapper">
	<div class="wrap">
		<img class="logo-project" src="/assets/img/projects/roombion/logo-roombion.png" alt="">
		<h2>Дизайн и разработка интернет-магазина</h2>
		<div class="bloc oblique">
			<div class="pattern-in">
				<p>Мы создали функциональный сайт с лаконичным дизайном.<br>
					Динамическая смена фотографий в шапке сайта. Быстрая навигация по основным разделам.</p>
				<img src="/assets/img/projects/roombion/page.jpg">
			</div>
		</div>
	</div>
	<div class="bd"><h4>Приятные мелочи</h4></div>
	<div class="slider-outer">
		<a class="prewButton" href="#"></a>
		<a class="nextButton" href="#"></a>
		<div class="slider pf">
			<div class="slider-wrap">
				<div class="slider-inner">
					<div class="slide"><img src="/assets/img/projects/roombion/1.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/roombion/2.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/roombion/3.jpg" alt=""></div>
				</div>
			</div>
			<div class="description">
				<div class="project-desc">
					<h6>Поиск по товарам в реальном времени.</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Динамический подбор товаров по цвету.</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Автоматическое определение местонахождения покупателя при вводе адреса.</h6>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="bloc">
			<h4>Личный кабинет администратора</h4>
			<p>Удобное добавление товаров. Управление каталогом сайта.</p>
			<img src="/assets/img/projects/roombion/admin.jpg" alt="">
		</div>
	</div>
	<div class="bd">
		<h4>Иконки в стиле Flat</h4>
		<p>Полная интеграция с платежными системами.</p>
	</div>
	<div class="bloc icons rmb">
		<img src="/assets/img/projects/roombion/icons.png" alt="">
	</div>
</div>
