<? /* NormaPravo */ ?>
<div class="wrapper">
	<div class="wrap">
		<img class="logo-project" src="/assets/img/projects/normapravo/logo-pravo.png" alt="">
		<h2>Лэндинг для юридического центра</h2>
	</div>

		<div class="bloc half">
			<img src="/assets/img/projects/normapravo/page.jpg" alt="">
		</div>

		<div class="slider-outer">
		<div class="slider pf">
			<a class="prewButton" href="#"></a>
			<a class="nextButton" href="#"></a>
			<div class="slider-wrap">
				<div class="slider-inner">
					<div class="slide"><img src="/assets/img/projects/normapravo/1.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/normapravo/2.jpg" alt=""></div>
				</div>
			</div>
			<div class="description">
				<div class="project-desc">
					<h6>При скролле акцентируется внимание на одном из тезисов.</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Динамическая шапка с кратким описанием основных направлений деятельности центра.</h6>
				</div>
			</div>
		</div>
	</div>
	<div class="bloc">
		<p>Реальные истории людей, обратившихся в центр для решения своих проблем<br> и комментарии юристов.</p>
		<img src="/assets/img/projects/normapravo/page2.jpg" alt="">
	</div>

</div>
