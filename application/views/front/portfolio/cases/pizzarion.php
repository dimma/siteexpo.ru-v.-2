<? /* Pizzarion */ ?>
<div class="wrapper">
	<div class="wrap">
		<img class="logo-project" src="/assets/img/projects/pizza/logo-pizza.png" alt="">
		<h2>Дизайн и разработка сайта для пиццерии</h2>
		<p>На странице представлены основные разделы каталога. Есть возможность быстрого добавления товаров в корзину. В шапке происходит динамическая смена товаров.</p>
		<div class="pattern">
			<div class="pattern-in">
				<img src="/assets/img/projects/pizza/page.jpg">
			</div>
		</div>
	</div>
	<div class="bd"><h4>Внутренние страницы</h4></div>
	<div class="slider-outer">
		<div class="slider pf">
			<a class="prewButton" href="#"></a>
			<a class="nextButton" href="#"></a>
			<div class="slider-wrap">
				<div class="slider-inner">
					<div class="slide"><img src="/assets/img/projects/pizza/1.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/pizza/2.jpg" alt=""></div>
					<div class="slide"><img src="/assets/img/projects/pizza/3.jpg" alt=""></div>
				</div>
			</div>
			<div class="description">
				<div class="project-desc">
					<h6>Страница товара с подробным описанием продукта,<br>
						возможностью увеличить фотографию товара при клике на него.</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Удобная авторизация пользователя по номеру телефона<br>
						или через социальные сети.</h6>
				</div>
				<div class="project-desc" style="visibility:hidden">
					<h6>Быстрое добавление товаров в корзину.<br>
						Количество товаров можно изменить прямо в корзине.</h6>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="bloc">
			<h4>Фотосъемка товаров</h4>
			<p>Мы провели профессиональную фотосъемку всех продуктов питания.</p>
			<ul>
				<li>
					<img src="/assets/img/projects/pizza/f1.png" alt="">
					<p>155 фотографий пиццы</p>
				</li>
				<li>
					<img src="/assets/img/projects/pizza/f2.png" alt="">
					<p>87 фотографий пирогов</p>
				</li>
				<li>
					<img src="/assets/img/projects/pizza/f3.png" alt="">
					<p>103 фотографии роллов</p>
				</li>
				<li>
					<img src="/assets/img/projects/pizza/f4.png" alt="">
					<p>91 фотография суши</p>
				</li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="bloc">
			<h4>Бонусная программа для постоянных клиентов</h4>
			<p>Дизайн бонусной карты и разработка механики бонусной программы.</p>
			<img src="/assets/img/projects/pizza/card.png">
		</div>
	</div>
	<div class="bd">
		<h4>Набор иконок для сайта</h4>
	</div>
	<div class="bloc icons pz">
		<img src="/assets/img/projects/pizza/icons.png" alt="">
	</div>
</div>
