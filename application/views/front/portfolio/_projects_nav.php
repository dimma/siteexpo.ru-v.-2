<div class="clear"></div>

<div class="control">
	<div class="wrap">
		<div class="target-link">
			<p><?=$case->link?></p>
			<? /* <img src="/assets/img/target-link.png" alt=""> */ ?>
		</div>
		<a class="target-portf" href="/portfolio"><img src="/assets/img/portf.png" alt=""><span>Все проекты</span></a>
		<? if($prev->id) { ?>
			<h4 class="target-prev"><?=$prev->name?></h4>
			<a class="target-prev" href="/portfolio/<?=$prev->url?>">&lt; Предыдущий проект</a>
		<? } ?>
		<? if($next->id) { ?>
			<h4 class="target-next"><?=$next->name?></h4>
			<a class="target-next" href="/portfolio/<?=$next->url?>">Следующий проект &gt;</a>
		<? } ?>
	</div>
</div>