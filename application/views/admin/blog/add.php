<?=Form::open('/admin/ajax/add_post', array('id' => 'add-post-form', 'class' => 'ajax-form'))?>
	<div class="form-group">
		<input name="title" class="form-control" type="text" placeholder="Заголовок">
	</div>
	<div class="form-group">
		<input name="url" class="form-control input-sm" type="text" placeholder="Псевдоним ссылки">
	</div>
	<div class="form-group upload-block">
		<div class="pull-left">
			<span class="btn fileinput-button">
			  <i class="icon-plus"></i>
			  <span>Загрузить изображение</span>
			  <input data-bfi-disabled multiple name="files" type="file" id="blog-upload">
			</span>
		</div>
		<div class="pull-left" id="post-images-list"></div>
		<div class="clearfix"></div>
		<div class="progress progress-small" id="blog-img-progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
	</div>
	<div class="form-group">
		<textarea name="text" class="form-control" placeholder="Текст записи"></textarea>
	</div>
	<div id="article-submit" class="form-actions form-actions-padding-sm form-actions-padding-md form-actions-padding-lg" style="margin-bottom: 0;">
		<button class="btn btn-success btn-lg"><i class="icon-save"></i>&nbsp;&nbsp;Добавить запись</button>
	</div>
<?=Form::close();?>
