<?=Form::open('/admin/ajax/add_case', array('id' => 'add-case-form', 'class' => 'ajax-form'))?>
	<div class="form-group">
		<input name="name" class="form-control input-sm" type="text" placeholder="Проект"/>
	</div>
	<div class="form-group">
		<input name="desc" class="form-control" type="text" placeholder="Описание"/>
	</div>
	<div class="form-group">
		<textarea name="url" class="form-control" placeholder="URL"></textarea>
	</div>
	<div class="form-group">
		<input name="link" class="form-control" type="text" placeholder="Ссылка"/>
	</div>
	<div id="article-submit" class="form-actions form-actions-padding-sm form-actions-padding-md form-actions-padding-lg" style="margin-bottom: 0;">
		<button class="btn btn-success btn-lg"><i class="icon-save"></i>&nbsp;&nbsp;Добавить кейс</button>
	</div>
<?=Form::close();?>