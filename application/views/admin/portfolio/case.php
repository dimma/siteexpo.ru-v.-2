<?=Form::open('/admin/ajax/edit_case', array('id' => 'add-case-form', 'class' => 'ajax-form'))?>
	<input type="hidden" name="id" value="<?=$case->id?>"/>
	<div class="form-group">
		<input name="name" class="form-control input-sm" type="text" placeholder="Проект" value="<?=$case->name?>"/>
	</div>
	<div class="form-group">
		<input name="desc" class="form-control" type="text" placeholder="Описание" value="<?=$case->desc?>"/>
	</div>
	<div class="form-group">
		<textarea name="url" class="form-control" placeholder="URL"><?=$case->url?></textarea>
	</div>
	<div class="form-group">
		<input name="link" class="form-control" type="text" placeholder="Ссылка" value="<?=$case->link?>"/>
	</div>
	<div id="article-submit" class="form-actions form-actions-padding-sm form-actions-padding-md form-actions-padding-lg" style="margin-bottom: 0;">
		<button class="btn btn-success btn-lg"><i class="icon-save"></i>&nbsp;&nbsp;Сохранить</button>
	</div>
<?=Form::close();?>