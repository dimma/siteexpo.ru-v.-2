<div class='row'>
	<div class='col-sm-12'>
		<div class='box'>
			<div class='box-content box-no-padding list-box'>
				<?php if ($cases->count()) : ?>
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="thin-col text-center">&#8470;</th>
								<th>Проект</th>
								<th>URL</th>
								<th>Статус</th>
								<th class="thin-col">Дата</th>
							</tr>
						</thead>
						<tbody id="cases-list">
							<?php foreach ($cases as $case) : ?>
								<tr post_id="<?=$case->id?>" id="case_<?=$case->id?>">
									<td class="text-center"><small class="request-id"><?=$case->id?></small></td>
									<td><?=$case->name?></td>
									<td><?=$case->url?></td>
									<td><?=$case->status?></td>
									<td>
										<small class="text-muted nowrap">
											<?php if (date('d.m.Y') == date('d.m.Y', strtotime($case->date))) : ?>
												Сегодня,&nbsp;<?=Date::format($case->date, 'H:i')?>
											<?php elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($case->date, 'Y-m-d')))->format('%a') == 1) : ?>
												Вчера,&nbsp;<?=Date::format($case->date, 'H:i')?>
											<?php elseif (date('Y') !== date('Y', strtotime($case->date))) : ?>
												<?=Date::format($case->date, 'd.m.Y')?>
											<?php else : ?>
												<?=Date::format($case->date, 'd F, H:i')?>
											<?php endif; ?>
										</small>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php else : ?>
					<p class="block-empty">Нет данных.</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>