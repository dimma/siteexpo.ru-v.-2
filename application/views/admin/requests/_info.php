<?=Form::open('/admin/ajax/request', array('id' => 'request-form'))?>
	<?=Form::hidden('request_id','')?>
	<div class="row">
		<div class="col-md-2">
			<strong>Имя:</strong>
		</div>
		<div class="col-md-5">
			<span><?=$request->name?></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<strong>Телефон:</strong>
		</div>
		<div class="col-md-5">
			<span><?=$request->phone?></span>
		</div>
	</div>
	<? if($request->email) { ?>
	<div class="row">
		<div class="col-md-2">
			<strong>E-mail:</strong>
		</div>
		<div class="col-md-5">
			<span><a href="mailto:<?=$request->email?>"><?=$request->email?></a></span>
		</div>
	</div>
	<? } ?>
	<? if($request->text) { ?>
	<div class="row request-text">
		<h3>Текст сообщения:</h3>
		<p><?=$request->text?></p>
	</div>
	<? } ?>
	<? if($request->category && $request->type) { ?>
		<div class="row">
			<div class="col-md-2">
				<strong>Категория:</strong>
			</div>
			<div class="col-md-5">
				<?=$labels[$request->category]?>&nbsp;&nbsp;
				<small class="table-stars">
					<? for($i = 1; $i <= $stars_count[$request->type]; $i++) { ?>
						<i class='icon-star'></i>
					<? } ?>
				</small>
			</div>
		</div>
	<? } ?>
	<? if($request->services) { $services = explode(',',$request->services); ?>
	<h3>Дополнительные услуги</h3>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Услуга</th>
				<th class="thin-col">Цена (руб.)</th>
			</tr>
		</thead>
		<tbody>
			<tr class="active">
				<td>Базовый набор услуг</td>
				<td></td>
			</tr>
		<? foreach(ORM::factory('service')->where('id','in',$services)->find_all() as $service) {?>
			<tr>
				<td><?=$service->name?></td>
				<td class="text-right"><?=$service->price?></td>
			</tr>
		<? } ?>
			<tr class="warning">
				<td class="text-right">ИТОГО:</td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<? } ?>

<?=Form::close()?>