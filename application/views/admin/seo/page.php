<?=Form::open('/admin/ajax/edit_page', array('id' => 'add-page-form', 'class' => 'ajax-form'))?>
	<input type="hidden" name="id" value="<?=$page->id?>"/>
	<div class="form-group">
		<input name="name" class="form-control input-sm" type="text" placeholder="Страница" value="<?=$page->name?>"/>
	</div>
	<div class="form-group">
		<input name="title" class="form-control" type="text" placeholder="Title" value="<?=$page->title?>"/>
	</div>
	<div class="form-group">
		<textarea name="description" class="form-control" placeholder="Descritpion"><?=$page->description?></textarea>
	</div>
	<div class="form-group">
		<input name="keywords" class="form-control" type="text" placeholder="Keywords" value="<?=$page->keywords?>"/>
	</div>
	<div id="article-submit" class="form-actions form-actions-padding-sm form-actions-padding-md form-actions-padding-lg" style="margin-bottom: 0;">
		<button class="btn btn-success btn-lg"><i class="icon-save"></i>&nbsp;&nbsp;Сохранить</button>
	</div>
<?=Form::close();?>