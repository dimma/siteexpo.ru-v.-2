<?=Form::open('/admin/ajax/add_page', array('id' => 'add-page-form', 'class' => 'ajax-form'))?>
	<div class="form-group">
		<input name="name" class="form-control input-sm" type="text" placeholder="Страница"/>
	</div>
	<div class="form-group">
		<input name="title" class="form-control" type="text" placeholder="Title"/>
	</div>
	<div class="form-group">
		<textarea name="description" class="form-control" placeholder="Descritpion"></textarea>
	</div>
	<div class="form-group">
		<input name="keywords" class="form-control" type="text" placeholder="Keywords"/>
	</div>
	<div id="article-submit" class="form-actions form-actions-padding-sm form-actions-padding-md form-actions-padding-lg" style="margin-bottom: 0;">
		<button class="btn btn-success btn-lg"><i class="icon-save"></i>&nbsp;&nbsp;Добавить</button>
	</div>
<?=Form::close();?>