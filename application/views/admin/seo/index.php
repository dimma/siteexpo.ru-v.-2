<div class='row'>
	<div class='col-sm-12'>
		<div class='box'>
			<div class='box-content box-no-padding list-box'>
				<?php if ($pages->count()) : ?>
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="thin-col">Страница</th>
								<th>Title</th>
								<th>Description</th>
								<th>Keywords</th>
							</tr>
						</thead>
						<tbody id="pages-list">
							<?php foreach ($pages as $page) : ?>
								<tr post_id="<?=$page->id?>" id="page_<?=$page->id?>">
									<td class="thin-col nowrap"><?=$page->name?></td>
									<td class="nowrap"><?=$page->title?></td>
									<td class="nowrap"><?=$page->description?></td>
									<td class="nowrap"><?=$page->keywords?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php else : ?>
					<p class="block-empty">Нет страниц</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>