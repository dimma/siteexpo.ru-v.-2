<!DOCTYPE html>
<head>
	<meta charset="utf-8"/>
	<title>Ошибка 404</title>
	<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
</head>
	<body class="error-page">
		<div class="fixed" id="nav">
			<div class="wrap">

				<ul>
					<li><a href="/"><img src="/assets/img/logo.svg" id="nav-logo-fixed" alt=""></a></li>

					<ul class="centering">
						<li><a href="/portfolio">Портфолио</a></li>
						<li><a href="/prices">Цены</a></li>
						<li><a href="/blog">Блог</a></li>
						<li><a href="/contacts">Контакты</a></li>
					</ul>

					<li class="entry-form arrow-corner">
						<a href="#">Оставить заявку</a>
					</li>
				</ul>
			</div>
		</div>

		<h1>404</h1>

		<div class="modal">
			<div class="modalBg" id="top-req-bg"></div>
			<div class="modalWin" id="top-req-win">
				<h2>Оставьте заявку</h2>
				<div class="form_input">
					<form action="/contact" method="post" accept-charset="utf-8" id="contact-form" class="contact-form">
						<p><label for="name">Имя</label><input type="text" name="name" placeholder=""></p>
						<p><label for="email">Почта</label><input type="text" name="email" placeholder=""></p>
						<p><label for="phone">Телефон</label><input type="text" class="phone" name="phone" placeholder="+7 987 654-32-10 — пример"></p>
						<button class="arrow-corner">Отправить заявку</button>
						<button class="_cansel">Отменить</button>
					</form>
				</div>
				<div class="form-errors">
					<p class="error-name error-txt">Укажите ваше имя</p>
					<p class="error-email error-txt">Неправильный адрес электронной почты</p>
					<p class="error-phone error-txt">Неправильный телефон</p>
				</div>
			</div>
		</div>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter24951449 = new Ya.Metrika({id:24951449,
							webvisor:true,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="//mc.yandex.ru/watch/24951449" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-50888095-1', 'auto');
			ga('send', 'pageview');
		</script>

		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>