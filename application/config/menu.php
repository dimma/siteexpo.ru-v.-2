<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'portfolio' => array(
		'url' => 'portfolio',
		'class' => 'portfolio',
		'name' => 'Портфолио',
		'title' => 'Портфолио',
	),
	'prices' => array(
		'url' => 'prices',
		'class' => 'prices',
		'name' => 'Цены',
		'title' => 'Цены',
	),
	'blog' => array(
		'url' => 'blog',
		'class' => 'blog',
		'name' => 'Блог',
		'title' => 'Блог',
	),
	'contacts' => array(
		'url' => 'contacts',
		'class' => 'contact',
		'name' => 'Контакты',
		'title' => 'Контакты',
	),
);
?>