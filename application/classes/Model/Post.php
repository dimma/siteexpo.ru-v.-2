<?php defined('SYSPATH') or die('No direct script access.');

class Model_Post extends ORM
{
	protected $_sorting = array('date' => 'DESC');

	public function rules()
	{
		return array(
			'title' => array(array('not_empty')),
			'text' => array(array('not_empty')),
		);
	}

	protected $_has_many = array(
		'images' => array('model' => 'Postimage', 'foreign_key' => 'post_id'),
	);
}
?>