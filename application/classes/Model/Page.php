<?php defined('SYSPATH') or die('No direct script access.');

class Model_Page extends ORM
{
	public function rules()
	{
		return array(
			'name' => array(
				array('not_empty'),
				array(array($this, 'unique'), array('name', ':value')),
			),
			'title' => array(array('not_empty')),
		);
	}
}
?>