<?php defined('SYSPATH') or die('No direct script access.');

class Model_Setting extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				array('Security::xss_clean'),
			),
		);
	}

	public function rules()
	{
		return array(
			'name' => array(array('not_empty')),
			'value' => array(array('not_empty')),
		);
	}
}
?>