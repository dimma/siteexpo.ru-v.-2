<?php defined('SYSPATH') or die('No direct script access.');

class Model_Request extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				array('Security::xss_clean'),
			),
		);
	}

	public function rules()
	{
		return array(
			'name' => array(array('not_empty')),
			'phone' => array(array('not_empty'), array('Valid::phone')),
			'email' => array(array('Valid::email'))
		);
	}

	protected $_sorting = array('date' => 'DESC');
}
?>