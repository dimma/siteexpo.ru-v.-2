<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Layout {

	# Main page
	public function action_index()
	{
		$data = array();

		$this->template->body_class = 'main';
		if($this->template->detect->is('iPhone') || $this->template->detect->is('iPad'))
		{
			$this->template->body_class .= ' apple';
		}
		$this->template->content = View::factory('front/main', $data);
	}

	public function action_contact()
	{
		if($this->request->post())
		{
			$post = $this->request->post();

			if($post['name'] !== '' && $post['phone'] !== '' && Valid::phone($post['phone'], 11))
			{
				if($post['email'] !== '' && !filter_var($post['email'], FILTER_VALIDATE_EMAIL))
				{
					$json_data['errors']['email'] = 'not_valid';
					$send = false;
				} else {
					$send = true;
				}

				$post['phone'] = preg_replace('/\D+/', '', $post['phone']);
				$post['phone'] = sprintf("%s %s %s-%s-%s",
					str_replace('8','+7',substr($post['phone'], 0, 1)),
					substr($post['phone'], 1, 3),
					substr($post['phone'], 4, 3),
					substr($post['phone'], 7, 2),
					substr($post['phone'], 9, 2)
				);

				if($send)
				{

					$text  = '<p>Поступила заявка с сайта <a href="http://siteexpo.ru">siteexpo.ru</a></p>';
					$text .= '<p>Имя: '.$post['name'].'<br />Телефон: '.$post['phone'].'<br />E-mail: '.$post['email'].'</p>';

					if(isset($post['text']))
					{
						$text .= '<p><strong>Текст сообщения:</strong><br />'.$post['text'].'</p>';
					}

					Email::factory('Заявка с сайта siteexpo.ru')
						//->to('info@xsoft.org')
						->to('rustam@otono.ru')
						//->cc('xmugutdin@xsoft.org')
						//->cc('info@siteexpo.ru')
						->from('robot@xsoft.org', 'Siteexpo.ru')
						->message($text, 'text/html')
						->send();

					# Save request
					$post['date'] = Date::formatted_time();
					ORM::factory('Request')->values($post)->save();

					$json_data['sent'] = 1;
				}
			}
			else
			{
				foreach($post as $k => $v)
				{
					if($v == '' && ($k == 'name' || $k == 'phone'))
					{
						$json_data['errors'][$k] = 'not_empty';
					}
				}

				if($post['email'] !== '' && !filter_var($post['email'], FILTER_VALIDATE_EMAIL))
				{
					$json_data['errors']['email'] = 'not_valid';
				}

				if(!Valid::phone($post['phone']))
				{
					$json_data['errors']['phone'] = 'not_valid';
				}
			}

			echo json_encode($json_data);
		}
		else
		{
			throw HTTP_Exception::factory('404');
		}

		exit;
	}
}