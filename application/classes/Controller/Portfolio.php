<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Portfolio extends Controller_Layout {

	# Portfolio
	public function action_index()
	{
		$data['cases'] = ORM::factory('Case')->where('status','=','1')->find_all();
		$this->template->content = View::factory('front/portfolio/index', $data);
	}

	# One project
	public function action_project()
	{
		$id = $this->request->param('id');
		$data['case'] = ORM::factory('Case', array('url' => $id, 'status' => 1));
		if($data['case']->id)
		{
			$data['prev'] = ORM::factory('Case')->where('id','<',$data['case']->id)->and_where('status','=','1')->order_by('id','DESC')->limit(1)->find();
			$data['next'] = ORM::factory('Case')->where('id','>',$data['case']->id)->and_where('status','=','1')->order_by('id','ASC')->limit(1)->find();
			$this->template->body_class = 'portfolio inner';
			$this->template->content = View::factory('front/portfolio/project', $data);
		}
		else
		{
			throw HTTP_Exception::factory(404);
		}
	}
}