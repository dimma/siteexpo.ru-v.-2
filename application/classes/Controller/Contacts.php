<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Contacts extends Controller_Layout {

	# Contacts
	public function action_index()
	{
		$data = array();
		$this->template->content = View::factory('front/contacts/index', $data);
	}
}