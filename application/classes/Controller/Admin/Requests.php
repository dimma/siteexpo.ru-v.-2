<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Requests extends Controller_Admin_Layout {

	public function action_index()
	{
		Controller::redirect('/admin/requests/new');
	}

	# Новые заявки
	public function action_new()
	{
		$this->template->title = '<small>Заявки</small> / Новые';
		$data['requests'] = ORM::factory('request')->where('status','=','0')->find_all();
		$data['archive'] = FALSE;
		$data['labels'] = array('shop' => 'Интернет магазин', 'corporate' => 'Корпоративный', 'startup' => 'Стартап');
		$data['stars_count'] = array(1 => 3, 2 => 4, 3 => 5);
		$this->template->content = View::factory('admin/requests/index', $data);
	}

	# Архив заявок
	public function action_archive()
	{
		$this->template->title = '<small>Заявки</small> / Архив';
		$data['requests'] = ORM::factory('request')->where('status','=','1')->find_all();
		$data['archive'] = TRUE;
		$data['labels'] = array('shop' => 'Интернет магазин', 'corporate' => 'Корпоративный', 'startup' => 'Стартап');
		$data['stars_count'] = array(1 => 3, 2 => 4, 3 => 5);
		$this->template->content = View::factory('admin/requests/index', $data);
	}

}
