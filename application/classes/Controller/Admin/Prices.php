<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Prices extends Controller_Admin_Layout {

	public function action_index()
	{
		Controller::redirect('/admin/prices/shop');
	}

	# Интернет магазин
	public function action_shop()
	{
		$this->template->title = '<small>Цены</small> / Интернет магазин';
		$data['category'] = 'shop';
//		$data['base_prices'] = DB::select('name','value')
//			->from('settings')
//			->where('name','in',array($data['category'].'_budget',$data['category'].'_standart',$data['category'].'_unique'))
//			->execute()->as_array('name');

		$data['base_prices'][1] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','1')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices'][2] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','2')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices'][3] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','3')->and_where('package','=','base')->execute()->as_array();

		$data['base_prices'][1] = $data['base_prices'][1][0]['sum'];
		$data['base_prices'][2] = $data['base_prices'][2][0]['sum'];
		$data['base_prices'][3] = $data['base_prices'][3][0]['sum'];

		$max_price_budget = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','1')->and_where('package','=','additional')->execute()->as_array();
		$max_price_standart = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','2')->and_where('package','=','additional')->execute()->as_array();
		$max_price_unique = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','3')->and_where('package','=','additional')->execute()->as_array();

		$data['max_prices'] = array(
			1 => $max_price_budget[0]['sum'] + $data['base_prices'][1],
			2 => $max_price_standart[0]['sum'] + $data['base_prices'][2],
			3 => $max_price_unique[0]['sum'] + $data['base_prices'][3],
		);

		$this->template->content = View::factory('admin/prices/index', $data);
	}

	# Корпоративный
	public function action_corporate()
	{
		$this->template->title = '<small>Цены</small> / Корпоративный сайт';
		$data['category'] = 'corporate';

		$data['base_prices'][1] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','1')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices'][2] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','2')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices'][3] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','3')->and_where('package','=','base')->execute()->as_array();

		$data['base_prices'][1] = $data['base_prices'][1][0]['sum'];
		$data['base_prices'][2] = $data['base_prices'][2][0]['sum'];
		$data['base_prices'][3] = $data['base_prices'][3][0]['sum'];

		$max_price_budget = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','1')->and_where('package','=','additional')->execute()->as_array();
		$max_price_standart = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','2')->and_where('package','=','additional')->execute()->as_array();
		$max_price_unique = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','3')->and_where('package','=','additional')->execute()->as_array();

		$data['max_prices'] = array(
			1 => $max_price_budget[0]['sum'] + $data['base_prices'][1],
			2 => $max_price_standart[0]['sum'] + $data['base_prices'][2],
			3 => $max_price_unique[0]['sum'] + $data['base_prices'][3],
		);

		$this->template->content = View::factory('admin/prices/index', $data);

	}

	# Стартап
	public function action_startup()
	{
		$this->template->title = '<small>Цены</small> / Стартап';
		$data['category'] = 'startup';

		$data['base_prices'][1] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','1')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices'][2] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','2')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices'][3] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','3')->and_where('package','=','base')->execute()->as_array();

		$data['base_prices'][1] = $data['base_prices'][1][0]['sum'];
		$data['base_prices'][2] = $data['base_prices'][2][0]['sum'];
		$data['base_prices'][3] = $data['base_prices'][3][0]['sum'];

		$max_price_budget = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','1')->and_where('package','=','additional')->execute()->as_array();
		$max_price_standart = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','2')->and_where('package','=','additional')->execute()->as_array();
		$max_price_unique = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=',$data['category'])->and_where('type','=','3')->and_where('package','=','additional')->execute()->as_array();

		$data['max_prices'] = array(
			1 => $max_price_budget[0]['sum'] + $data['base_prices'][1],
			2 => $max_price_standart[0]['sum'] + $data['base_prices'][1],
			3 => $max_price_unique[0]['sum'] + $data['base_prices'][1],
		);

		$this->template->content = View::factory('admin/prices/index', $data);
	}
}
