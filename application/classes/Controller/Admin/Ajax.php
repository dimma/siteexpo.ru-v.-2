<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Ajax extends Controller_Admin_Adminajax {

	public function action_index()
	{
		echo '==';
		exit;
	}

	// Add service
	public function action_add_service()
	{
		if($this->request->post())
		{
			$post = $this->request->post();
			$post['sort_order'] = ORM::factory('Service')->where('category','=',$post['category'])->and_where('type','=',$post['type'])->and_where('package','=',$post['package'])->count_all() + 1;
			$post['date'] = Date::formatted_time();

			if(!is_numeric($post['price']))
			{
				unset($post['price']);
			}

			if($post['name'] !== '')
			{
				$service = ORM::factory('Service')->values($post)->save();
				$json_data = array(
					'sort_order' => $service->sort_order,
					'id' => $service->id
				);

				echo json_encode($json_data);
			}
		}

		exit;
	}

	// Delete service
	public function action_delete_service()
	{
		if($this->request->post('id'))
		{
			ORM::factory('service', $this->request->post('id'))->delete();
		}

		exit;
	}

	// Edit service
	public function action_edit_service()
	{
		if($this->request->post('service_id'))
		{
			$post = $this->request->post();
			$post['price'] = (isset($post['price']) && is_numeric($post['price'])) ? $post['price'] : 0;

			ORM::factory('Service', $post['service_id'])->values($post)->update();
		}

		exit;
	}

	// Base service price
	public function action_set_base_price()
	{
		if($this->request->post('value'))
		{
			$post = $this->request->post();
			$post['value'] = str_replace(' ','',$post['value']);
			ORM::factory('Setting', array('name' => $post['name']))->values(array('value' => $post['value']))->update();
		}
		exit;
	}

	// Sort service
	public function action_sort_service()
	{
		if($this->request->post('id') && $this->request->post('toPosition'))
		{
			$service_id = str_replace('service_','',$this->request->post('id'));

			$service = ORM::factory('service', $service_id);
			$to = $this->request->post('toPosition');
			$from = $this->request->post('fromPosition');

			if($this->request->post('direction') == 'forward')
			{
				DB::update('services')->set(array('sort_order' => DB::expr('sort_order - 1')))->where('sort_order','>=',$from)->and_where('sort_order','<=',$to)->and_where('id','<>',$service_id)->and_where('category','=',$service->category)->and_where('type','=',$service->type)->and_where('package','=',$service->package)->execute();
			}
			elseif($this->request->post('direction') == 'back')
			{
				DB::update('services')->set(array('sort_order' => DB::expr('sort_order + 1')))->where('sort_order','>=',$to)->and_where('sort_order','<=',$from)->and_where('id','<>',$service_id)->and_where('category','=',$service->category)->and_where('type','=',$service->type)->and_where('package','=',$service->package)->execute();
			}

			$service->values(array('sort_order' => $to))->update();
		}

		exit;
	}

	// Archive request
	public function action_archive_request()
	{
		if($this->request->post())
		{
			Kohana::ar($this->request->post());
			ORM::factory('Request', $this->request->post('request_id'))->values(array('status' => '1'))->update();
		}

		exit;
	}

	// Request info
	public function action_request_info()
	{
		if($this->request->post())
		{
			$data['request'] = ORM::factory('Request', $this->request->post('id'));
			$data['labels'] = array('shop' => 'Интернет магазин', 'corporate' => 'Корпоративный', 'startup' => 'Стартап');
			$data['stars_count'] = array(1 => 3, 2 => 4, 3 => 5);

			echo View::factory('admin/requests/_info', $data);
		}

		exit;
	}

	// Add post
	public function action_add_post()
	{
		if(isset($_FILES['files']['tmp_name']))
		{
			$type = key($_FILES);
			$name = md5(rand(11111111,99999999).time());
			$ext = strtolower(pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION));
			$file_name = $name.'.'.$ext;

			Upload::save($_FILES[$type], $file_name, DOCROOT.'assets/upload/blog/_temp');

			/*
			$file_data = array(
				'name' => $name,
				'file' => $file_name,
				'ext' => $ext,
				'date' => Date::formatted_time(),
			);

			$image = ORM::factory('Postimage')->values($file_data)->save();
			*/

			$json = array(
				'src' => '/assets/upload/blog/_temp/'.$file_name,
				'file' => $file_name
			);

			echo json_encode($json);
		}
		elseif($this->request->post() && !$_FILES)
		{
			try
			{
				$_post = $this->request->post();
				$_post['date'] = Date::formatted_time();
				$_post['status'] = 1;

				$post = ORM::factory('Post')->values($_post)->save();

				// Adding name and title in SEO tools page
				ORM::factory('Page')->values(array('name' => 'blog/' . $post->url, 'title' => $post->title))->save();

				if($this->request->post('images'))
				{
					foreach($this->request->post('images') as $image)
					{
						copy(DOCROOT.'assets/upload/blog/_temp/'.$image, DOCROOT.'assets/upload/blog/'.$image);
						unlink(DOCROOT.'assets/upload/blog/_temp/'.$image);

						$a_name = explode('.',$image);
						ORM::factory('Postimage')->values(array(
							'name' => $a_name[0],
							'ext' => strtolower(pathinfo($image, PATHINFO_EXTENSION)),
							'file' => $image,
							'date' => Date::formatted_time(),
							'post_id' => $post->id
						))->save();
					}
				}

				$json['status'] = 1;
			}
			catch(ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
				$json['status'] = 0;
			}

			echo json_encode($json);
		}

		exit;
	}

	// Update post
	public function action_update_post()
	{
		if(isset($_FILES['files']['tmp_name']))
		{
			$type = key($_FILES);
			$name = md5(rand(11111111,99999999).time());
			$ext = strtolower(pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION));
			$file_name = $name.'.'.$ext;

			Upload::save($_FILES[$type], $file_name, DOCROOT.'assets/upload/blog/_temp');

			/*
			$file_data = array(
				'name' => $name,
				'file' => $file_name,
				'ext' => $ext,
				'date' => Date::formatted_time(),
			);

			$image = ORM::factory('Postimage')->values($file_data)->save();
			*/

			$json = array(
				'src' => '/assets/upload/blog/_temp/'.$file_name,
				'file' => $file_name
			);

			echo json_encode($json);
		}
		elseif($this->request->post() && !$_FILES)
		{
			try
			{
				$_post = $this->request->post();

				$post = ORM::factory('Post', $this->request->post('post_id'));
				$oldUri = $post->url;
				$post->values($_post)->update();

				// Updating name and title in SEO tools page
				$postSeo = ORM::factory('Page')->where('name', '=', 'blog/' . $oldUri)->find();
				if (!empty($postSeo->id) && $oldUri !== $this->request->post('url'))
				{
					$page = ORM::factory('Page', $postSeo->id);
					$page->values(array('name' => 'blog/' . $post->url, 'title' => $post->title))->update();
				}

				if($this->request->post('images'))
				{
					foreach($this->request->post('images') as $image)
					{
						copy(DOCROOT.'assets/upload/blog/_temp/'.$image, DOCROOT.'assets/upload/blog/'.$image);
						unlink(DOCROOT.'assets/upload/blog/_temp/'.$image);

						$a_name = explode('.',$image);
						ORM::factory('Postimage')->values(array(
							'name' => $a_name[0],
							'ext' => strtolower(pathinfo($image, PATHINFO_EXTENSION)),
							'file' => $image,
							'date' => Date::formatted_time(),
							'post_id' => $post->id
						))->save();
					}
				}

				$json['status'] = 1;
			}
			catch(ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
				$json['status'] = 0;
			}

			echo json_encode($json);
		}


		exit;
	}

	/**
	 * Add page for SEO tools
	 * @access public
	 * return string
	 */
	public function action_add_page()
	{
		$post = $this->request->post();
		$json = array('status' => 0);

		if ($post)
		{
			try
			{
				ORM::factory('Page')->values($post)->save();
				$json['status'] = 1;
			}
			catch (ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
			}

			echo json_encode($json);
		}

		exit;
	}

	/**
	 * Update page for SEO tools
	 * @access public
	 * return string
	 */
	public function action_edit_page()
	{
		$post = $this->request->post();
		$json = array('status' => 0);

		if ($post)
		{
			try
			{
				$page = ORM::factory('Page', $post['id']);
				$page->values($post)->update();
				$json['status'] = 1;
			}
			catch (ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
			}

			echo json_encode($json);
		}

		exit;
	}

	/**
	 * Add portfolio case
	 * @access public
	 * return string
	 */
	public function action_add_case()
	{
		$post = $this->request->post();
		$json = array('status' => 0);

		if ($post)
		{
			try
			{
				$post['status'] = 1;
				$post['date'] = Date::formatted_time();
				ORM::factory('Case')->values($post)->save();
				$json['status'] = 1;

				// Adding name and title in SEO tools page
				$title = $post['name'] . ((empty($post['desc'])) ? '' : '. ' . $post['desc']);
				ORM::factory('Page')->values(array('name' => 'portfolio/' . $post['url'], 'title' => $title))->save();

				// Add view template for portfolio case
				fopen('./application/views/front/portfolio/cases/' . $post['url'] . '.php', 'a');

			}
			catch (ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
			}

			echo json_encode($json);
		}

		exit;
	}

	/**
	 * Update portfolio case
	 * @access public
	 * return string
	 */
	public function action_edit_case()
	{
		$post = $this->request->post();
		$json = array('status' => 0);

		if ($post)
		{
			try
			{
				$case = ORM::factory('Case', $post['id']);
				$oldUri = $case->url;
				$case->values($post)->update();

				// Updating name and title in SEO tools page
				$postSeo = ORM::factory('Page')->where('name', '=', 'portfolio/' . $oldUri)->find();
				if (!empty($postSeo->id) && $oldUri !== $post['url'])
				{
					$title = $post['name'] . ((empty($post['desc'])) ? '' : '. ' . $post['desc']);
					$page = ORM::factory('Page', $postSeo->id);
					$page->values(array('name' => 'portfolio/' . $post['url'], 'title' => $title))->update();
				}

				$json['status'] = 1;
			}
			catch (ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
			}

			echo json_encode($json);
		}

		exit;
	}
}
