<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Portfolio extends Controller_Admin_Layout {

	public function action_index()
	{
		$data = array();
		$data['cases'] = ORM::factory('Case')->find_all();
		$this->template->title_content = View::factory('admin/portfolio/_top');
		$this->template->content = View::factory('admin/portfolio/index', $data);
	}

	/**
	 * Add portfolio case
	 */
	public function action_add()
	{
		$data = array();
		$this->template->title = '<small><a href="/admin/portfolio">Портфолио</a></small> / Добавить кейс';
		$this->template->content = View::factory('admin/portfolio/add', $data);
	}

	/**
	 * Edit portfolio case
	 */
	public function action_case()
	{
		$data = array('case' => ORM::factory('Case', $this->request->param('id')));
		$this->template->title = '<small><a href="/admin/portfolio">Редактировать кейс</a></small> / ' . $data['case']->name;
		$this->template->content = View::factory('admin/portfolio/case', $data);
	}
}