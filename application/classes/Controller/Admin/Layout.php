<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Layout extends Controller_Template
{
	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'admin/layout';

	public function before()
	{
		parent::before();

		# Смотрим, залогинен ли юзер
		$auth = Auth::instance();

		if($this->request->controller() !== 'Recovery')
		{
			if($auth->logged_in())
			{
				$this->user = Auth::instance()->get_user();
			}
			# Если нет, то просим его авторизоваться
			else
			{
				if($this->request->controller() !== 'Auth' && $this->request->action() !== 'login')
				{
					Controller::redirect('/admin/login');
				}
			}
		}
		else
		{
			$this->user = ($auth->logged_in()) ? Auth::instance()->get_user() : FALSE;
		}

		# Параметры шаблона
		if($this->auto_render)
		{
			//$detect = new MobileDetect();

			$this->template->title				= '';
			$this->template->window_title		= '';
			$this->template->meta_keywords		= '';
			$this->template->meta_description	= '';
			$this->template->meta_copyright		= '';
			$this->template->meta_author		= '';
			$this->template->styles				= array();
			$this->template->scripts			= array();
			$this->template->controller			= UTF8::strtolower($this->request->controller());
			$this->template->action				= $this->request->action();
			$this->template->main_page			= ($this->request->controller() == 'main' && $this->request->action() == 'index') ? TRUE : FALSE;
			$this->template->content			= '';
			$this->template->menu				= array();
			$this->template->user_title			= ($auth->logged_in() && isset($this->user)) ? $this->user->name : FALSE;
			$this->template->user				= ($auth->logged_in() && isset($this->user)) ? $this->user : FALSE;
			$this->template->icon               = '';
			$this->template->rights				= array();
			$this->template->title_content		= '';
			$this->template->digits				= array();

			# Indicators
			$this->template->digits['requests'] = ORM::factory('request')->where('status','=','0')->count_all();

			# Rights
			if($this->template->user)
			{
				foreach(ORM::factory('Role')->find_all() as $role)
				{
					$this->template->rights[$role->name] = FALSE;
				}

				foreach($this->user->roles->find_all() as $role)
				{
					$this->template->rights[$role->name] = TRUE;
				}
			}

			# Menu
			foreach(Kohana::$config->load('admin_menu') as $k => $v)
			{
				$this->template->menu[$k] = $v;
			}

			# Current page
			$menu = Kohana::$config->load('admin_menu');

			if(isset($menu[$this->template->controller]))
			{
				$this->template->title = $menu[$this->template->controller]['name'];
				$this->template->window_title = $menu[$this->template->controller]['name'].' | Site Expo admin';
				//$this->template->icon = $menu[$this->template->controller]['icon'];
			}
		}
	}

	public function after()
	{
		if($this->auto_render)
		{
			# Подключаем стандартные стили и скрипты
			$styles['assets/admin/stylesheets/bootstrap/bootstrap.css'] = 'screen';
			$styles['assets/admin/stylesheets/light-theme.css'] = 'screen';
			$styles['assets/admin/stylesheets/theme-colors.css'] = 'screen';
			$styles['assets/admin/stylesheets//plugins/jquery_fileupload/jquery.fileupload-ui.css'] = 'screen';
			$styles['assets/admin/stylesheets/layout.css'] = 'screen';

			$scripts[] = 'assets/admin/javascripts/jquery/jquery-1.11.0.min.js';
			$scripts[] = 'assets/admin/javascripts/jquery/jquery.mobile.custom.min.js';
			$scripts[] = 'assets/admin/javascripts/jquery/jquery-migrate.min.js';
			$scripts[] = 'assets/admin/javascripts/jquery/jquery-ui-1.10.4.custom.min.js';
			$scripts[] = 'assets/admin/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js';
			$scripts[] = 'assets/admin/javascripts/bootstrap/bootstrap.js';
			$scripts[] = 'assets/admin/javascripts/plugins/modernizr/modernizr.min.js';
			$scripts[] = 'assets/admin/javascripts/plugins/retina/retina.js';
			$scripts[] = 'assets/admin/javascripts/jquery.dataTables.min.js';
			$scripts[] = 'assets/admin/javascripts/jquery.dataTables.rowReordering.js';
			$scripts[] = 'assets/admin/javascripts/theme.js';

			if($this->template->controller == 'blog')
			{
				$scripts[] = 'assets/admin/javascripts/theme.js';
				$scripts[] = 'assets/admin/javascripts/plugins/fileupload/jquery.iframe-transport.min.js';
				$scripts[] = 'assets/admin/javascripts/plugins/fileupload/jquery.fileupload.min.js';
				$scripts[] = 'assets/admin/javascripts/plugins/fileupload/jquery.fileupload-ui.min.js';
			}

			$scripts[] = 'assets/admin/javascripts/common.js?v=3';

			$this->template->styles		= array_merge($this->template->styles, $styles);
			$this->template->scripts	= array_merge($this->template->scripts, $scripts);
		}

		parent::after();
	}
}
?>