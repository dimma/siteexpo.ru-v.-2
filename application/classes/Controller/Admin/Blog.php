<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Blog extends Controller_Admin_Layout {

	public function action_index()
	{
		$data['posts'] = ORM::factory('Post')->find_all();
		$data['post_statuses'] = array(
			'1' => 'Опубликовано',
			'2' => 'Скрыто'
		);
		$this->template->title_content = View::factory('admin/blog/_top');
		$this->template->content = View::factory('admin/blog/index', $data);
	}

	# Article
	public function action_post()
	{
		$id = $this->request->param('id');
		$data['post'] = ORM::factory('Post', $id);

		$this->template->title = '<small><a href="/admin/blog">Блог</a></small> / '.$data['post']->title;
		$this->template->content = View::factory('admin/blog/post', $data);
	}

	# Add article
	public function action_add()
	{
		$data = array();

		$this->template->title = '<small><a href="/admin/blog">Блог</a></small> / Добавить запись';
		$this->template->content = View::factory('admin/blog/add', $data);
	}
}