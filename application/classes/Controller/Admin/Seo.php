<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Seo extends Controller_Admin_Layout {

	/**
	 * List of pages
	 */
	public function action_index()
	{
		$data = array();
		$data['pages'] = ORM::factory('Page')->find_all();
		$this->template->title_content = View::factory('admin/seo/_top');
		$this->template->content = View::factory('admin/seo/index', $data);
	}

	/**
	 * Edit page
	 */
	public function action_page()
	{
		$data = array('page' => ORM::factory('Page', $this->request->param('id')));
		$this->template->title = '<small><a href="/admin/seo">Инструменты SEO</a></small> / ' . $data['page']->name;
		$this->template->content = View::factory('admin/seo/page', $data);
	}

	/**
	 * Add new page
	 */
	public function action_add()
	{
		$data = array();
		$this->template->title = '<small><a href="/admin/seo">Инструменты SEO</a></small> / Добавить страницу';
		$this->template->content = View::factory('admin/seo/add', $data);
	}
}