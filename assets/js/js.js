$(document).ready(function(){

	//Fix Nav
	var navTop = $('#nav');

 	$(window).scroll(function(){
 		if($('body').hasClass('main'))
 		{
	        if ( $(this).scrollTop() > 56 && navTop.hasClass('default') ){
	            navTop.removeClass('default').addClass('fixed');
	            $('#nav-logo-fixed').attr('src','/assets/img/logoFix.svg');
	        } 
	        else if($(this).scrollTop() <= 56 && navTop.hasClass('fixed')) {
	            navTop.removeClass('fixed').addClass('default');
	            $('#nav-logo-fixed').attr('src','/assets/img/logo.svg');
	        }

			//$('section#preview').css({ 'background-position': 'center -' + ($(document).scrollTop() - $('#preview').offset().top) * 0.5 + 'px' });
    	}
    });

    //Modal Win
	var popWin = $('#top-req-win');
	var popBg = $('#top-req-bg');
	var body = $('body');

	$('.entry-form').click(function(){
		popBg.fadeIn(250);
		popWin.fadeIn(250);
		$('.contact-form input[type="text"]:eq(0)').focus();
		body.css({'overflow':'hidden'});
		return false;
	});

	$(window).resize(function(){
		popWin.css({
				'position' :'fixed',
				'left':($(window).width() - popWin.outerWidth())/2,
				'top':($(window).height() - popWin.outerHeight())/2
		});
	});
	$(window).resize();


	$('._cansel').click(function() {
		body.css({'overflow':'auto'});
	    $('.modalWin').fadeOut(150);
	    $('.modalBg').fadeOut(150);
	    return false;
	});
	$(document).bind('keyup', function(e) {
		if(e.keyCode==27) {
			body.css({'overflow':'auto'});
			$('.modalWin').fadeOut(150);
			$('.modalBg').fadeOut(150);
		}
	});

	//Slider
	var hwSlideSpeed = 200;
	var hwTimeOut = 3500;
	var hwNeedLinks = true;
	 
	$('.slide').css({
    	"position" : "absolute",
        "top":'0', "left": '0'
	}).hide().eq(0).show();

	    var slideNum = 0;
	    var slideTime;
	    slideCount = $('.slider-inner .slide').size();
	    var animSlide = function(arrow){
	        clearTimeout(slideTime);
	        $('.slide').eq(slideNum).fadeOut(hwSlideSpeed);
	        if(arrow == "next"){
	            if(slideNum == (slideCount-1)){slideNum=0;}
	            else{slideNum++}
	       	}
			else if(arrow == "prew")
			{
	            if(slideNum == 0){slideNum=slideCount-1;}
	            else{slideNum-=1}
	        }
	        else{
	            slideNum = arrow;
	        }

	        $('.project-desc').hide();
			$('.project-desc:eq('+slideNum+')').fadeIn('fast');

	        $('.project-desc').css('visibility','hidden');
			$('.project-desc:eq('+slideNum+')').css('visibility','visible');


	        $('.slide').eq(slideNum).fadeIn(hwSlideSpeed/*, rotator*/);
	        $('.control-slide.active').removeClass('active');
	        $('.control-slide').eq(slideNum).addClass('active');
	    }

		if(hwNeedLinks){
		    $('.nextButton').click(function(){
		        animSlide("next");
		 		return false;
		        })
		    $('.prewButton').click(function(){
		        animSlide("prew");
		        return false;
		        })
		}
	    var $adderSpan = '';
	    $('.slide').each(function(index) {
	        $adderSpan += '<span class = "control-slide">' + index + '</span>';
	    });
	    $('<div class ="sli-links">' + $adderSpan +'</div>').appendTo('#portfolio, .slider.pf');
	    $('.control-slide:first').addClass('active');
	     
	    $('.control-slide').click(function(){
	    	var goToNum = parseFloat($(this).text());
	    	animSlide(goToNum);
	    });
	    var pause = false;
	    /*var rotator = function(){
	    	if(!pause){slideTime = setTimeout(function(){animSlide('next')}, hwTimeOut);}
	        }
	    $('#slider-wrap').hover(    
	        function(){clearTimeout(slideTime); pause = true;},
	        function(){pause = false; rotator();});
		$('.entry-form').click(function()
	    	{clearTimeout(slideTime); pause = true;});
	    rotator();*/

	//Map Google
	    function init_map()
	    {
		var map_canvas = document.getElementById('contacts-map');
		var map_options = {
			center: new google.maps.LatLng(55.749585, 37.5372791),
			zoom: 16,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.BOTTOM_RIGHT
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			panControl: false
		}
		var map = new google.maps.Map(map_canvas, map_options)
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(55.749585, 37.5372791),
			map: map,
			icon: '/assets/img/float.png'
		});

	}

	if($('#contacts-map').length > 0)
	{
		google.maps.event.addDomListener(window, 'load', init_map);
	}

	//Masonry (Cascading grid layout library)
	$('#portfolio-list img')
		.each(function(){
			$(this).attr('src',$(this).attr('src')+'?'+new Date().getTime())
		})
		.load(function(){
			$('.block-preview').masonry({
			// указываем элемент-контейнер в котором расположены блоки для динамической верстки
	    	itemSelector: '.block-preview li',
			// указываем класс элемента являющегося блоком в нашей сетке
	    	singleMode: true,
			// true - если у вас все блоки одинаковой ширины
	    	isResizable: true,
			// перестраивает блоки при изменении размеров окна
	    	isAnimated: true,
			// анимируем перестроение блоков
	        animationOptions: {
	        queue: false,
	        duration: 500
	      }
	// опции анимации - очередь и продолжительность анимации
		});
	});

	//Pic Blog
	$('section .pic img').load(function(){
		changePic();
	});
	function changePic(){
		$('section .pic img').each(function(){
			var top = ($(this).height() - $(this).parent('section .pic').height())/2;
			$(this).css({
				'top': - top + 'px'
			})
		});
	};
	changePic();

	//Description Txt
	$(window).resize(function(){
		$('.project-desc').each(function(i){
			$(this).css({
				'top' : ($('.description').height() - $(this).height())/2 + 'px', 
				'left': ($('.description').width() - $(this).width())/2 + 'px'
			})
		});
	});
	$(window).resize();

	//
	$('.icons img').load(function(){
		loadPic();
	});
	function loadPic(){
		$('.icons img').each(function(){
			$(this).css({
				'position':'absolute',
				'top' : ($('.icons').height() - $(this).height())/2 + 'px', 
				'left': ($('.icons').width() - $(this).width())/2 + 'px'
			})
		});
	};
	$(window).resize(function(){
		loadPic();
	});

});