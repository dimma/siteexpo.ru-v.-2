// Contact form
// uses jquery
!function ($) {
	$(function(){
		// Send request
		$('.contact-form').submit(function(){
			var self = this;
			$(self).find('button.arrow-corner').animate({ opacity: 0.5 }, 200, function(){ $(self).find('button.arrow-corner').text('Заявка отправляется') }).attr('disabled','disabled').addClass('disabled').removeClass('send-success');

			$.post($(self).attr('action'), $(self).serialize(), function(data){
				$('#contact-success').hide();
				$('#valid-mail').hide();
				$(self).find('.error').removeClass('error');
				$(self).parents('.modalWin').find('.form-errors p').hide();

				var response = $.parseJSON(data);

				if(response.errors)
				{
					// Empty fields
					for(var n in response.errors)
					{
						$(self).find('input[name="'+n+'"]').addClass('error');
						$(self).find('textarea[name="'+n+'"]').addClass('error');
						$(self).parents('.modalWin').find('.error-'+n).show();
					}

					// Invalid e-mail
					if(response.errors['email'] == 'not_valid')
					{
						$('#valid-mail').show();
					}

					$('#contact-errors').show();
					$(self).find('button.arrow-corner').animate({ opacity: 1 }, 200, function(){ $(self).find('button.arrow-corner').text('Отправить заявку') }).removeAttr('disabled').removeClass('disabled');
				} else if(response.sent && response.sent == 1) {
					yaCounter24951449.reachGoal('request');
					$('#contact-errors').hide();

					if($('#contact-loader').length > 0 && $(self).hasClass('footer-form'))
					{
						$('#contact-loader').show();
						$('#contact-loader').animate({ width: 100+'%' }, 3000, function(){
							$('#contact-loader').fadeOut(function(){
								$('#contact-loader').css('width','1px');
							});
							$(self).find('button').removeClass('disabled').addClass('send-success').html('<img src="/assets/img/submitCheck.png">');
							setTimeout(function(){
								$(self).find('button').animate({ opacity: 1 }, 200, function(){
									$(self).find('button').removeAttr('disabled').removeClass('send-success');
								}).text('Отправить заявку');
								$(self).find('input').val('');
								$(self).find('textarea').val('');
							}, 2000);
						});
					} else {
						$(self).find('button').removeClass('disabled').addClass('send-success').html('<img src="/assets/img/submitCheck.png">');
						$(self).find('button').animate({ opacity: 1 }, 200, function(){
							$(self).find('button').removeAttr('disabled').removeClass('send-success');
						}).text('Отправить заявку');
						$(self).find('input').val('');
						$(self).find('textarea').val('');

						var popWin = $('.modalWin');
						var popBg = $('.modalBg');
						var body = $('body');
						var btn = $('.zakaz > div > button');

						$('#modal-request').removeClass('flipped');
						btn.removeClass('active');
						body.css({'overflow':'auto'});
						popWin.fadeOut(150);
						popBg.fadeOut(150);
					}
				}
			});

			return false;
		});
	})
}(window.jQuery)