$(document).ready(function () {
	// Show info
	$('ul.rate li').click(function(){
		var i = $('ul.rate li').index(this);
		var width = $('.bd').width();

		$('.details-info').css('width', width);

		var triangle_positions = new Array();
		if(width >= 900)
		{
			triangle_positions[0] = 140;
			triangle_positions[1] = 440;
			triangle_positions[2] = 740;
		} else if(width < 900) {
			triangle_positions[0] = 100;
			triangle_positions[1] = 320;
			triangle_positions[2] = 540;
		}

		//$('ul.rate li.active').removeClass('active');
		//$('ul.rate li:eq('+i+')').addClass('active');

		if($('ul.rate li.active-list').length > 0 && !$(this).hasClass('active-list'))
		{
			$('ul.rate li.active-list .fotoImg:eq(1)').fadeOut();
			$('ul.rate li.active-list .fotoImg:eq(0)').fadeIn();
			$('ul.rate li.active-list').removeClass('active-list');
		}

		$('ul.rate li:eq('+i+')').addClass('active-list');
		$('ul.rate li:eq('+i+') .fotoImg:eq(0)').fadeOut();
		$('ul.rate li:eq('+i+') .fotoImg:eq(1)').fadeIn();

		if(!$('#details-wrap').hasClass('opened'))
		{
			$('#details-wrap').addClass('opened');
			$('#details-wrap').show();

			$('#triangle').show();

			$('html, body').animate({ scrollTop: $('ul.rate').offset().top-47 }, 500);
			$('#details-wrap').css({ left: -((width+150)*i) });
			$('#triangle').css({ left: triangle_positions[i]+'px' });
			$('#details-wrap').css('height', $('#tab'+(i+1)).height());
		} else {
			$('#details-wrap').animate({ left: -((width+150)*i) }, 700, 'easeOutBack');
			$('#triangle').animate({ left: triangle_positions[i]+'px' }, 700, 'easeOutBack', function(){ $('#details-wrap').css('height', $('#tab'+(i+1)).height()); });
		}
	});

	/* center Footer Iphone */
	function set_iphone_footer()
	{
		var k = ($(window).height() > 320) ? 80 : 40;

		$('#contacts-footer').css({
			'position':'relative',
			'top': ($(window).height() - $('.footer.iphone').outerHeight())/2 - k + 'px'
		});

		$('#prices-footer').css({
			'position': 'relative',
			'top': 40 + 'px'
		});

	}
	set_iphone_footer();
	$(window).resize(function() {
		set_iphone_footer();
	});
	$(window).resize();

	/* pagination */
	$(window).resize(function() {
		$('#pagination').css({
			'position':'relative',
			'left':($(window).width() - $('#pagination').outerWidth())/2 + 'px'
		});
	});
	$(window).resize();

	/* Показать услуги */
	$('span.show').click(function(){
		$(this).parent('.look-detail').children('.hd').toggleClass('active');
		var text = ($(this).parent('.look-detail').children('span.show').text() == 'Скрыть') ? 'Показать услуги' : 'Скрыть';
		$(this).text(text);
	});

	$(window).resize(function(){
		$('.slideContent-List').find('.details-info .detail').css({
			'position' :'relative',
			'left':($(window).width() - $('.slideContent-List').find('.details-info .detail').outerWidth())/2
		});

		if($('.bd').length > 0)
		{
			var width = $('.bd').width();
			var i = $('ul.rate li').index($('ul.rate li.active-list'));
			var triangle_positions = new Array();

			if(width >= 900)
			{
				triangle_positions[0] = 140;
				triangle_positions[1] = 440;
				triangle_positions[2] = 740;
			} else if(width < 900) {
				triangle_positions[0] = 100;
				triangle_positions[1] = 320;
				triangle_positions[2] = 540;
			}

			$('.details-info').css('width', width);
			$('#details-wrap').css({ left: -((width+150)*i) });
			$('#triangle').css({ left: triangle_positions[i]+'px' });
		}
	});
	$(window).resize();


	/* Prices */
	function calc_price(s_price, list)
	{
		var total_price = parseInt($(s_price).attr('base_price'));

		$(list).find('input:checked').each(function(){
			//console.log(total_price+' + '+$(this).attr('price'));
			total_price += parseInt($(this).attr('price'));
		});

		//total_price += parseInt($(s_price).attr('base_price'));

		$(s_price).text(total_price.toFixed().replace(/./g, function(c, i, a) {
			return i && c !== "." && !((a.length - i) % 3) ? ' ' + c : c;
		}));
	}

	$('ul._add li input[type="checkbox"]').click(function(){
		var i_section = $('.details-info').index($(this).parents('.details-info'));
		var i = $('.details-info:eq('+i_section+') ul._add').index($(this).parents('ul._add'));
		var s_price = $(this).parents('.details-info').find('.total-price:eq('+i+')');
		var list = $(this).parents('ul._add');

		if($(list).find('input[type="checkbox"]').length !== $(list).find('input[type="checkbox"]:checked').length)
		{
			$('.details-info:eq('+i_section+') input.additional:eq('+i+')').removeAttr('checked').addClass('allChecked');
		} else {
			$('.details-info:eq('+i_section+') input.additional:eq('+i+')').prop('checked', true).removeClass('allChecked');
		}

		if($(list).find('input[type="checkbox"]:checked').length == 0)
		{
			$('.details-info:eq('+i_section+') input.additional:eq('+i+')').removeAttr('checked').removeClass('allChecked');
		}

		calc_price(s_price, list);
	});

	$('input.additional').click(function(){
		var i_section = $('.details-info').index($(this).parents('.details-info'));
		var i = $('.details-info:eq('+i_section+') input.additional').index(this);
		var s_price = $(this).parents('.details-info').find('.total-price:eq('+i+')');
		var list = $('.details-info:eq('+i_section+') ul._add:eq('+i+')');

		$(this).removeClass('allChecked');

		if($(this).is(':checked'))
		{
			$(list).find('input[type="checkbox"]').prop('checked',true);
		} else {
			$(list).find('input[type="checkbox"]').removeAttr('checked');
		}

		calc_price(s_price, list);
	});

	/* MODAL WIN */

	var popWin = $('#modal-request');
	var popBg = $('#modal-request-bg');
	var body = $('body');
	var btn = $('.zakaz > div > button');
	var HeightDoc = $(document).height();
	var WidthDoc = $(document).width();

	btn.click(function() {
		var index = $(this).parents('.details-info').find('.zakaz button').index(this);
		var index_site = $('.details-info').index($(this).parents('.details-info'));

		var title_type = $(this).parents('.details-info').find('h4._title:eq('+index+')').text().toLowerCase();
		var title_site = $('.rate h4:eq('+index_site+')').text().toLowerCase();
		title_site = (title_site == 'корпоративный') ? title_site+' сайт' : title_site;

		if(index == 0) {
			stars_count = 3;
		} else if(index == 1) {
			stars_count = 4;
		} else if(index == 2) {
			stars_count = 5;
		}

		$('#modal-request .site-type').text(title_type);
		$('#modal-request .site-category').text(title_site);
		$('#modal-request .stars span').attr('class','star s-'+stars_count);

		$(this).addClass('active');

		popWin.css({
			'position' :'fixed',
			'left':($(window).width() - popWin.outerWidth())/2,
			'top':($(window).height() - popWin.outerHeight())/2
		});

		popBg.fadeIn(250);
		popWin.fadeIn(250);
		body.css({'overflow':'hidden'});


		/* Checked services */
		var i = $('.details-info').index($(this).parents('.details-info'));
		var j = $('.details-info:eq('+i+') .zakaz button').index(this);

		var service_list = '';
		$('.details-info:eq('+i+') ul._add:eq('+j+') input:checked').each(function(){
			sep = (service_list !== '') ? ',' : '';
			service_list += sep+$(this).attr('service_id');
		});

		$('.contact-form input[name="services"]').val(service_list);
		$('.contact-form input[name="category"]').val($(this).attr('category'));
		$('.contact-form input[name="type"]').val($(this).attr('type'));

		if(!popWin.hasClass('iphone'))
		{
			$('#modal-request').addClass('flipped');
			$('#modal-request input[type="text"]:eq(0)').focus();
		}
	});

});
