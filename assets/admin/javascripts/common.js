!function ($) {
	$(function(){
		// Ajax forms
		function init_ajax_forms()
		{
			$('.ajax-form').unbind().submit(function(){
				var self = this;

				$.post($(this).attr('action'), $(this).serialize(), function(data){
					var response = $.parseJSON(data);

					$(self).find('.has-error').removeClass('error');
					$(self).find('.help-block').hide();

					if(response.errors)
					{
						for(var n in response.errors)
						{
							$(self).find('*[name="'+n+'"]').parents('.form-group').addClass('has-error');
							$(self).find('*[name="'+n+'"]').parents('.form-group').find('.help-block').show();
						}
					}

					if(response.status == 1)
					{
						var formId = $(self).attr('id');

						// Add post
						if (formId == 'add-post-form')
						{
							window.location = '/admin/blog';
						}

						// Add page
						if (formId == 'add-page-form')
						{
							window.location = '/admin/seo';
						}

						// Add page
						if (formId == 'add-case-form')
						{
							window.location = '/admin/portfolio';
						}
					}

				});
				return false;
			});
		}
		init_ajax_forms();

		// Number format
		function number_format(number) {
			number += '';
			x = number.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ' ' + '$2');
			}
			return x1 + x2;
		}

		// Escape quotes
		function addslashes( str ) {
			return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
		}

		// Service edit
		function init_service_edit()
		{
			// Edit service
			$('.prices-list input[type="text"]')
				.unbind()
				.blur(function(){
					update_price(this);
				})
				.focus(function(){
					$(this).parents('tr').addClass('edit-mode');
				})
				.keydown(function(event){
					switch(event.keyCode)
					{
						case 13:
							$(this).blur();
							return false;
							break
					}
				})
				.click(function(){
					$(this).select();
				});

			// Delete service
			$('.delete-service').unbind().click(function(){
				if(confirm('Вы уверены, что хотите удалить услугу "'+$(this).parents('tr').find('input[type="text"]:eq(0)').val()+'"'))
				{
					var service_id = $(this).parent().parent().attr('service_id');
					$.post('/admin/ajax/delete_service', { id: service_id });
					$(this).parent().parent().remove();
				}
			});
		}

		init_service_edit();

		function update_price(service)
		{
			var id = $(service).parent().parent().attr('service_id');
			var name = $(service).parent().parent().find('input[name="service['+id+'][name]"]').val();
			var price = $(service).parent().parent().find('input[name="service['+id+'][price]"]').val();
			var type = $(service).parents('.prices-list').attr('type');
			var package = $(service).parents('.prices-list').attr('package');
			$(service).parents('tr').removeClass('edit-mode');
			$.post('/admin/ajax/edit_service', { service_id: id, name: name, price: price }, function(){});

			// Count sum
			var sum_base = 0;
			$('.base-services[type="'+type+'"] .s-price').each(function(){
				sum_base += parseFloat($(this).val());
			});

			var sum_additional = 0;
			$('.additional-services[type="'+type+'"] .s-price').each(function(){
				sum_additional += parseFloat($(this).val());
			});

			$('.max-sum[type="'+type+'"]').text(number_format(sum_additional+sum_base));

			var sum = (package == 'base') ? sum_base : sum_additional;
			$('.'+package+'-sum[type="'+type+'"]').text(number_format(sum));
		}

		// Add service
		$('.add-service-form').submit(function(){
			var form = this;
			var type = $(form).find('input[name="type"]').val();
			var name = $(form).find('input[name="name"]').val().replace(/"/g,'&quot;');
			var price = $(form).find('input[name="price"]').val();

			$.post($(form).attr('action'), $(form).serialize(), function(data){
				var response = $.parseJSON(data);
				var service_id = response.id;
				$(form).find('.error').removeClass('error');
				if(service_id)
				{
					if($(form).find('select[name="package"]').val() == 'additional')
					{
						var row_class = ($('.prices-list[type="'+type+'"][package="additional"]').hasClass('odd')) ? 'even' : 'odd';
						$('.prices-list[type="'+type+'"][package="additional"]').append('' +
							'<tr data-position="'+response.sort_order+'" service_id="'+service_id+'" id="service_'+service_id+'" role="row" class="'+row_class+'">' +
							'<td class="sorting">'+response.sort_order+'</td>' +
							'<td class="thin-col"><i class="icon-resize-vertical"></i></td>' +
							'<td><input type="text" name="service['+service_id+'][name]" value="'+name+'" class="form-control"></td>' +
							'<td class="thin-col"><input type="text" name="service['+service_id+'][price]" value="'+price+'" class="form-control text-right s-price"></td>' +
							'<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>' +
							'</tr>'
						);
					}
					else if($(form).find('select[name="package"]').val() == 'base')
					{
						var row_class = ($('.prices-list[type="'+type+'"][package="base"] tr:last-child').hasClass('odd')) ? 'even' : 'odd';
						$('.prices-list[type="'+type+'"][package="base"]').append('' +
							'<tr data-position="'+response.sort_order+'" service_id="'+service_id+'" id="service_'+service_id+'" role="row" class="'+row_class+'">' +
							'<td class="sorting">'+response.sort_order+'</td>' +
							'<td class="thin-col"><i class="icon-resize-vertical"></i></td>' +
							'<td><input type="text" name="service['+service_id+'][name]" value="'+name+'" class="form-control"></td>' +
							'<td class="thin-col"><input type="text" name="service['+service_id+'][price]" value="'+price+'" class="form-control text-right s-price"></td>' +
							'<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>' +
							'</tr>'
						);
					}

					$(form).find('input[name="name"]').val('');
				} else {
					$(form).find('input[name="name"]').addClass('error');
				}

				init_service_edit();
			});

			return false;
		});

//		$('select[name="package"]').change(function(){
//			if($(this).val() == 'base')
//			{
//				$(this).parents('tr').find('.add-service-price').hide();
//			} else {
//				$(this).parents('tr').find('.add-service-price').show();
//			}
//			$(this).parents('tr').find('input[name="name"]').focus();
//		});

		// Base price
		$('.base-price').blur(function(){
			var field = $(this).attr('name');
			var price = $(this).val();
			$.post('/admin/ajax/set_base_price', { name: field, value: price });
		});

		// Archive request
		$('.request-toarchive').click(function(){
			var request_id = $(this).parents('tr').attr('request_id');
			var row = $(this).parents('tr');
			$.post('/admin/ajax/archive_request', { request_id: request_id });

			$('.tooltip').remove();
			$(row).fadeOut('fast', function(){
				$(row).remove();

				if($('#requests-list tr').length > 0)
				{
					$('#requests-count').text($('#requests-list tr').length);
				} else {
					$('#requests-count').hide();
				}
			});

			return false;
		});

		// Request info
		$('#requests-list tr').click(function(){
			var request_id = $(this).find('.request-id').text();
			$('#modal-request-id').text(request_id);
			$('#modal-content').load('/admin/ajax/request_info', { id: request_id });
		});

		// Row reordering
		$('.prices-table').dataTable({
			paging: false,
			ordering: true,
			info: false,
			sDom: ''
		}).rowReordering({
			sURL: '/admin/ajax/sort_service',
			sRequestType: 'POST'
		});

		// ========================================================================================================== //
		// Blog																										  //
		// ========================================================================================================== //

		function init_add_article()
		{
			var b_height = $('header').height() + $('.page-header').height() + $('#footer').height() + $('.upload-block').height() + $('#add-post-form input[name="title"]').parent().outerHeight() + $('#add-post-form input[name="url"]').parent().outerHeight() + $('#article-submit').outerHeight() + 100;
			$('#add-post-form textarea').css('height', $(window).height() - b_height);
		}

		if($('#add-post-form').length > 0)
		{
			init_add_article();
			$('#add-post-form input[type="text"]:eq(0)').focus();
		}

		$(window).resize(function(){
			init_add_article();
		});

		$('#posts-list tr').click(function(){
			window.location = '/admin/blog/'+$(this).attr('post_id');
		});

		$('#pages-list tr').click(function(){
			window.location = '/admin/seo/' + $(this).attr('post_id');
		});

		$('#cases-list tr').click(function(){
			window.location = '/admin/portfolio/' + $(this).attr('post_id');
		});

		if($('#post-images-list').length > 0)
		{
			var timeout;

			function init_image_popover()
			{
				$('.img-src').mouseup(function(){
					$(this).select();
				});

				$('.popover').unbind().hover(
					function(){
						clearTimeout(timeout);
					},
					function(){
						$(this).remove();
					}
				);
			}

			function init_post_images()
			{
				$('#post-images-list img')
					.unbind()
					.hover(
						function(){
							clearTimeout(timeout);
							$('.popover').remove();
							var top = $(this).offset().top + $(this).height();
							var left = $(this).offset().left - 200;
							var img_src = "<img src='"+$(this).attr('data-content')+"' alt=''>";
							$('body').append('<div class="popover blog-preview fade bottom in" style="display: block; top: '+top+'px; left: '+left+'px;"><h3 class="popover-title"><input type="text" class="form-control img-src" value="'+img_src+'"></h3><div class="popover-content"><img src="'+$(this).attr('data-content')+'"></div></div>');
							//$('.popover').css('left', left - ($('.popover').width() / 2));
							init_image_popover();
						},
						function(){
							timeout = setTimeout(function(){
								$('.popover').remove();
							}, 1000);
						}
				);

			}

			init_post_images();

			$('#blog-upload').fileupload({
				dataType: 'json',
				formData: function(form){
					return form.serializeArray();
				},
				add: function(e, data){
					data.submit();
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#blog-img-progress .progress-bar-success').css(
						'width',
						progress + '%'
					).text(progress + '%');
				},
				done: function (e, data) {
					//console.log(data.result.id);
					$('#blog-img-progress .progress-bar-success').css('width', '0').text('');
					$('#post-images-list').append('<img src="'+data.result.src+'" data-content="'+data.result.src+'" alt=""><input type="hidden" name="images[]" value="'+data.result.file+'">');
					init_post_images();
				}
			});

		}
	})
}(window.jQuery)